@starwars @starwars-search
Feature: Starwars Regression checks - Search

  Background: 
    Given the user is on the Disney "starwars_us" page

  Scenario: View Starwars.com page - search bar
    Scenario_label=Starwars.com Search page_Search bar
    Then "Starwars icon" is displayed
    And "Search bar" is displayed

  Scenario: View Starwars.com page - Search auto complete dropdown
    Scenario_label=Starwars.com Search_AutoCompleteDropdown
    When user clicks on "Search icon"
    And user enters the text "yoda" in the "Search bar"
    Then "Auto complete" dropdown displays and contains the "yoda" results

  Scenario: View Starwars.com page - Search page Results
    Scenario_label=Starwars.com Search_AutoCompleteViewAll
    When user clicks on "Search icon"
    And user enters the text "yoda" in the "Search bar" and press Enter
    Then search results page contains the search text "yoda"

  Scenario: View Starwars.com page - Search for Game
    Scenario_label=Starwars.com Search_Game
    When user clicks on "Search icon"
    And user enters the text "LEGO STAR WARS III" in the "Search bar"
    And "Auto complete" dropdown displays and contains the "LEGO STAR WARS III" results
    And user clicks on any of the "Games apps" content results
    Then user navigate to the selected "Games apps" result page

  Scenario: View Starwars.com page - Search for video
    Scenario_label=Starwars.com Search_Video
    When user clicks on "Search icon"
    And user enters the text "THAT�S NOT HOW THE FORCE WORKS" in the "Search bar"
    And "Auto complete" dropdown displays and contains the "THAT�S NOT HOW THE FORCE WORKS" results
    And user clicks on any of the "Video" content results
    Then user navigate to the selected "Video" result page

  Scenario: View Starwars.com page - Search for databank
    Scenario_label=Starwars.com Search_Databank
    When user clicks on "Search icon"
    And user enters the text "CHEWBACCA" in the "Search bar"
    And "Auto complete" dropdown displays and contains the "CHEWBACCA" results
    And user clicks on any of the "Databank" content results
    Then user navigate to the selected "Databank" result page

  Scenario: View Starwars.com page - Refine Search News
    Scenario_label=Starwars.com Search_Refine Search News
    When user clicks on "Search icon"
    And user enters the text "asia" in the "Search bar" and press Enter
    And user clicks on the "News" refine search
    Then search results only displays "News" content results

  Scenario: View Starwars.com page - Refine Search Video
    Scenario_label=Starwars.com Search_Refine Search Video
    When user clicks on "Search icon"
    And user enters the text "force" in the "Search bar" and press Enter
    And user clicks on the "Video" refine search
    Then search results only displays "Video" content results

  Scenario: View Starwars.com page - Refine Search Databank
    Scenario_label=Starwars.com Search_Refine Search Databank
    When user clicks on "Search icon"
    And user enters the text "disney" in the "Search bar" and press Enter
    And user clicks on the "Databank" refine search
    Then search results only displays "Databank" content results

  Scenario: View Starwars.com page - Refine Search Galleries
    Scenario_label=Starwars.com Search_Refine Search Galleries
    When user clicks on "Search icon"
    And user enters the text "force" in the "Search bar" and press Enter
    And user clicks on the "Galleries" refine search
    Then search results only displays "Galleries" content results

  Scenario: View Starwars.com page - Refine Search TV-Shows
    Scenario_label=Starwars.com Search_Refine Search TV-Shows
    When user clicks on "Search icon"
    And user enters the text "force" in the "Search bar" and press Enter
    And user clicks on the "TV-Shows" refine search
    Then search results only displays "TV-Shows" content results

  Scenario: View Starwars.com page - Refine Search Films
    Scenario_label=Starwars.com Search_Refine Search Films
    When user clicks on "Search icon"
    And user enters the text "death" in the "Search bar" and press Enter
    And user clicks on the "Films" refine search
    Then search results only displays "Films" content results
