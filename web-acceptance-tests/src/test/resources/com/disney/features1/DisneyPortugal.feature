@disney_portugal
Feature: 027 - Disney - Portugal Home Page Functionalities

	Background: 
    When the user is on the Disney "disney_portugal" page

  @cookie @cookieWarningAppearance
  Scenario: Cookie Warning Appearance
    When a regional cookie warning overlay appears at the bottom of the page
    And cookie appears in the same language as the TLD
    And cookie contains a link to the Disney "privacy" Policy page in that language
    And cookie contains a clickable Accept button

  @cookie @noCookieWarning
  Scenario: Cookie Warning Does Not Appear
    When user clicks Accept button
    Then a regional cookie warning overlay does not appear at the bottom of the page

  @globalChrome
  Scenario: Clicking on the Chrome drop down menu expands the window
    When the user clicks on the Disney "chrome dropdown" button
    And the "chrome" "menu" expands open
    Then the user is able to click on the Disney "chrome up arrow" button to minimize the Chrome

  #@globalChrome
  #Scenario: Clicking on the Chrome drop down menu allows the user to click on any item located within
    #When the user clicks on the Disney "chrome dropdown" button
    #And the "chrome" "menu" expands open
    #And user clicks on the "menuitems" "list" and will be navigated to the related page

  @legalFooters
  Scenario: Footer links display and function as intended
    When the user scrolls to the bottom of the page
    Then the user can verify the "footerelements" "list" and will be navigated to the related page

  @adsDisplay 
  Scenario: Ads load on the page
    When user confirms the number of "displayads" of "disney_portugal" "ads"

  @search
  Scenario: User is on Disney home page and clicks on the search field located on the chrome
    When clicks on the "search" "text" field located on the chrome with input text as "fru"
    Then user will be redirected to a new page with their search results "frui"

  @autoCompleteSearch
  Scenario: Users types in an incomplete phrase in the search field and the search shows an autocomplete suggestion
    When clicks on the "search" "text" field located on the chrome with input text "fru"
    Then the "autocompletesearch" "input" will display an autocomplete suggestion

  @searchResultsPageFilters
  Scenario: User is able to select different filters to narrow their search results to a specific set of content
    When clicks on the "search" "text" field located on the chrome with input text "fru"
    Then the "autocompletesearch" "input" will display an autocomplete suggestion
    And clicks on one of the search filters which contains the text "fru"
    #Then the search will show items related only to the filtere text "fru"

  @searchResultsPageMoreFilters
  Scenario: User is able to to display more search results when clicking on the 'Show more' button
    When clicks on the "search" "text" field located on the chrome with input text "fru"
    Then the "autocompletesearch" "input" will display an autocomplete suggestion
    And user clicks on the view all results link
    #Then the search will display additional search results for the input text "fru"
    