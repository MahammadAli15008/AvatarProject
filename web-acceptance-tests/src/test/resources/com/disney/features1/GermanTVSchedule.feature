@GermanTVSchedule
Feature: 038 - German TV Schedule Content Loads

  Background: 
    When the user is on the Disney "german_TV" page

	@cookie @cookieWarningAppearance
  Scenario: Cookie Warning Appearance
    When a regional cookie warning overlay appears at the bottom of the page
    And cookie appears in the same language as the TLD
    And cookie contains a clickable Accept button

  @cookie @noCookieWarning
  Scenario: Cookie Warning Does Not Appear
    When user clicks Accept button
    Then a regional cookie warning overlay does not appear at the bottom of the page
   
	Scenario: Validate German TV Schedule Content Loads and by default current date is selected
		Then the user should be presented with the "filters" "calendar" 
		And validate if today, previous and tomorrow dates are available in the "dates" "calendar"
		And validate todays date is selected in the "dates" "calendar"
		Then user should view the "content" related to today's "date"
	 
	Scenario: Validate German TV Schedule Content Loads for earlier date than today 
		When user scroll down the page
		Then the user can view the "filters" "calendar" at the top 
		When user clicks on previous date from the "dates" "calendar"
		Then user should view the "content" related to the previous "date"
	 
	Scenario: Validate German TV Schedule Content Loads for later date than today 
		When user scroll down the page
		Then the user can view the "filters" "calendar" at the top 
		When user clicks on tomorrows date from the "dates" "calendar"
		Then user should view the "content" related to tomorrow's "date"
	 
	Scenario: Validate German TV Schedule Content Loads for todays date when selected
		When user scroll down the page
		Then the user can view the "filters" "calendar" at the top 
		When user clicks on todays date from the "dates" "calendar"
		Then user should view the "content" related to today's "date"
