@disney_us_search
Feature: 040 - Disney - US Search 

  Background: 
	Given the user is on the Disney "disney_us_search" page 
	
  Scenario: Autocomplete returns results 
  Scenario_label=Search_AutoComplete
	And user enters "m" in the "search" "text" field 
	Then results should be returned in the dropdown 
	Then results should have an anchor link 

  Scenario: Show More Button Loads Content 
  Scenario_label=Search_ShowMore
	When clicks on the "search" "text" field located on the chrome with input text as "mickey" 
	And user clicks on the "show more" "button" 
	Then the page should display additional results for the input text "mickey" 

  Scenario: Shop items in Everything filter contains affiliate codes 
  Scenario_label=Search_ShopItems
	When clicks on the "search" "text" field located on the chrome with input text as "mickey" 
	Then all "store results column" "link" contain the "cmp=OTL-Dcom&att=DcomM_Search_Feed_mickey" 

  Scenario: Store Filter returns objects and contain affliciate code 
  Scenario_label=Search_StoreFilter
	When clicks on the "search" "text" field located on the chrome with input text as "mickey" 
	And user clicks on the "Store" "button" 
	Then all "storefilter" "link" contain the "cmp=OTL-Dcom&att=DcomM_Search_Organic_mickey" 
	And user clicks on the "show more" "button" 
	Then all "storefilter" "link" contain the "cmp=OTL-Dcom&att=DcomM_Search_Organic_mickey" 

  Scenario: Parks Filter returns objects 
  Scenario_label=Search_ParksFilter
	When clicks on the "search" "text" field located on the chrome with input text as "mickey" 
	And user clicks on the "Parks" "button" 
	Then the page should display additional results for the input text "Parks+%26+Travel&q=mickey" 
	And user clicks on the "show more" "button" 
	Then the page should display additional results for the input text "Parks+%26+Travel&q=mickey" 

  Scenario: Video Filter returns objects 
  Scenario_label=Search_VideoFilter
	When clicks on the "search" "text" field located on the chrome with input text as "mickey" 
	And user clicks on the "Videos" "button" 
	Then the page should display additional results for the input text "Videos&q=mickey" 
	And user clicks on the "show more" "button" 
	Then the page should display additional results for the input text "Videos&q=mickey" 

  Scenario: Games Filter returns objects 
  Scenario_label=Search_GamesFilter
	When clicks on the "search" "text" field located on the chrome with input text as "game" 
	And user clicks on the "Games" "button" 
	Then the page should display additional results for the input text "Games&q=game" 
	And user clicks on the "show more" "button"
	Then the page should display additional results for the input text "Games&q=game"