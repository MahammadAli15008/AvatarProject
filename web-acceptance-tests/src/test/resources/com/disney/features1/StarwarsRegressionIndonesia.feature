@starwars @starwars-id
Feature: Starwars Regression checks - Indonesia

  Background: 
    Given the user is on the Disney "starwars_id" page

  Scenario: Footer link "Terms of Use" navigation
    Scenario_label=Navigation_Terms of Use_Indo
    When user clicks on "Syarat Penggunaan" from "Footer" list
    Then user navigates to "Terms of Use" page

  Scenario: Footer link "Privacy Policy" navigation
    Scenario_label=Navigation_Privacy Policy_Indo
    When user clicks on "Kebijakan Privasi" from "Footer" list
    Then user navigates to "Privacy Policy" page

  Scenario: Footer link "Star Wars Helpdesk" navigation
    Scenario_label=Navigation_Star Wars Helpdesk_Indo
    When user clicks on "Bantuan Star Wars" from "Footer" list
    Then user navigates to "Star Wars Helpdesk" page

  Scenario: View Starwars Home page
    Scenario_label=Starwars.com page_Search bar_Indo
    Then "Starwars icon" is displayed
    And "Search bar" is displayed

  Scenario: View News Page - categories
    Scenario_label=News page_Categories_Indo
    When user clicks on "news submenu"
    Then "news page" is displayed
    And "news categories" are displayed

  #Scenario: View Events Page - categories
  #Scenario_label=Events page_Categories_Indo
  #When user clicks on "event submenu"
  #Then "event page" is displayed
  #And "event categories" are displayed
  
  Scenario: View Film Page - Synopsis
    Scenario_label=Film page_Synopsis_Indo
    When user clicks on "film submenu"
    Then "film page" is displayed
    When user clicks on "some film"
    Then "film synopsis" is displayed

  Scenario: View Video Page - watch video
    Scenario_label=StarWars_Indo_VideoMenu
    When user clicks on "video submenu"
    Then "video page" is displayed
    And user clicks on "play icon" to play the video
    And user select the "video" from the playlist
    Then user navigates to the "watch video" page of the video

  Scenario: View TV-Shows Page - select article
    Scenario_label=StarWars_Indo_TV-ShowsMenu
    When user clicks on "TV-shows submenu"
    Then "TV-shows page" is displayed
    And user clicks on the "series article"
    Then "series article" page should get displayed

  Scenario: View Games Page - select article
    Scenario_label=StarWars_Indo_GamesMenu
    When user clicks on "Games submenu"
    Then "Games page" is displayed
    And user clicks on the "Interactive article"
    Then "Interactive article" page should get displayed

  Scenario: View Community Page - select article
    Scenario_label=StarWars_Indo_CommunityMenu
    When user clicks on "community submenu"
    Then "community page" is displayed
    And user clicks on the "community article"
    Then "community article" page should get displayed

  Scenario: View Databank Page - select article
    Scenario_label=StarWars_Indo_DatabankMenu
    When user clicks on "databank submenu"
    Then "databank page" is displayed
    And user clicks on the "databank article"
    Then "databank article" page should get displayed

  Scenario: Social link "Youtube" navigation
    Scenario_label=Navigation_Youtube_Indo
    When user clicks on "Youtube"
    Then user navigates to "Youtube" page
