@starwars @starwars-urls
Feature: Starwars Regression checks - URLs redirect to the US Starwars site.

  Background: 
    Given the user is on the Disney "starwars_us" page

  Scenario: Starwars Sunsetted "https://en-hk_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_hkURL_to_Starwars.com
    When the user enters the starwars "https://en-hk_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted "https://fi_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_FinlandURL_to_Starwars.com
    When the user enters the starwars "https://fi_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted " http://sea_starwars_com-qa.fe.mh.disney.io/" URL navigation
    Scenario_label=Navigation_SeaURL_to_Starwars.com
    When the user enters the starwars " http://sea_starwars_com-qa.fe.mh.disney.io/" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted " http://nl_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_NetherlandURL_to_Starwars.com
    When the user enters the starwars " http://nl_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted "http://dk_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_DenmarkURL_to_Starwars.com
    When the user enters the starwars "http://dk_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted " http://no_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_NorwayURL_to_Starwars.com
    When the user enters the starwars " http://no_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted "http://tr_starwars_com-qa.fe.mh.disney.io/gucgunu" URL navigation
    Scenario_label=Navigation_TurkeyURL_to_Starwars.com
    When the user enters the starwars "http://tr_starwars_com-qa.fe.mh.disney.io/gucgunu" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted "http://se_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_SwedenURL_to_Starwars.com
    When the user enters the starwars "http://se_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted "http://es_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_SpainURL_to_Starwars.com
    When the user enters the starwars "http://es_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted "http://fr_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_FranceURL_to_Starwars.com
    When the user enters the starwars "http://fr_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted "http://it_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_ItalyURL_to_Starwars.com
    When the user enters the starwars "http://it_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted "http://uk_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_UK URL_to_Starwars.com
    When the user enters the starwars "http://uk_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page

  Scenario: Starwars Sunsetted "http://zh-hk_starwars_com-qa.fe.mh.disney.io/findtheforce" URL navigation
    Scenario_label=Navigation_ZhURL_to_Starwars.com
    When the user enters the starwars "http://zh-hk_starwars_com-qa.fe.mh.disney.io/findtheforce" URL
    Then user redirect to the "starwars_us" page
