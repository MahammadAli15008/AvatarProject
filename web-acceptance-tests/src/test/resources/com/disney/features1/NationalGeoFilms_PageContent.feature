@natgeo-pagecontent
Feature: National Geographic Films - ADA & Page Content Tests

  Background: 
    Given the user is on the "natgeo_films" page

  @natgeo
  Scenario: Verify all the images should contain 'alt or aria-label' tag.
    Then all the images should have alt or aria-label tag

  @natgeo-prod
  Scenario: Page content 'Promo banner' Left navigation
    And user click on the "Promo banner" "left arrow" button
    Then user should navigates to the "Promo banner" "Next" slide

  @natgeo-prod
  Scenario: Page content 'Promo banner' Right navigation
    And user click on the "Promo banner" "right arrow" button
    Then user should navigates to the "Promo banner" "Previous" slide

  @natgeo
  Scenario: Page content 'Video Slider' Left navigation
    And user click on the "Video Slider" "left arrow" button
    Then user should navigates to the "Video Slider" "Next" slide

  @natgeo
  Scenario: Verify the images should display in 'Video Slider'
    Then user should view the images under "Video Slider" module

  @natgeo
  Scenario: Page content 'Video Slider' slider navigation
    And user click on the "1" "slide" on the Video Slider
    Then user can view "1" "slide" video should be play

  @natgeo
  Scenario: Navigates to 'Facebook' Page through center link
    And user click on the "Facebook" "center link"
    Then user should navigates to the "Facebook" page

  @natgeo
  Scenario: Navigates to 'Twitter' Page through center link
    And user click on the "Twitter" "center link"
    Then user should navigates to the "Twitter" page

  @natgeo
  Scenario: Navigates to 'Instagram' Page through center link
    And user click on the "Instagram" "center link"
    Then user should navigates to the "Instagram" page

  @natgeo
  Scenario: Page content 'Movie Poster' slider navigation
    And user click on the "1" "Movie" on the Movie Poster
    Then user should navigates to the "1" "Movie" page
