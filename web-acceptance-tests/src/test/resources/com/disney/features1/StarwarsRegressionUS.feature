@starwars @starwars-us
Feature: Starwars Regression checks - US

  Background: 
    Given the user is on the Disney "starwars_us" page

  Scenario: View Starwars.com page
    Scenario_label=Starwars.com page_Search bar
    Then "Starwars icon" is displayed
    And "Search bar" is displayed

  Scenario: View News Page - categories
    Scenario_label=News page_Categories_US
    When user clicks on "news submenu"
    Then "news page" is displayed
    And "news categories" are displayed

  Scenario: View News Page - archive
    Scenario_label=News page_Archive_US
    When user clicks on "news submenu"
    Then "news page" is displayed
    When user clicks on "archive page"
    Then user views archive article

  Scenario: View News Page - archive year
    Scenario_label=News page_Archive year_US
    When user clicks on "news submenu"
    Then "news page" is displayed
    When user clicks on "2014" from "Year" list
    Then user navigates to "2014" page

  #Scenario: View Events Page - categories
  #Scenario_label=Events page_Categories_US
  #When user clicks on "event submenu"
  #Then "event page" is displayed
  #And "event categories" are displayed
  
  Scenario: View Film Page - Synopsis
    Scenario_label=Film page_Synopsis_US
    When user clicks on "film submenu"
    Then "film page" is displayed
    When user clicks on "some film"
    Then "film synopsis" is displayed

  Scenario: View Film Page - Buy channels
    Scenario_label=Film page_Buy channels_US
    When user clicks on "film submenu"
    Then "film page" is displayed
    When user clicks on "some film"
    And user clicks on "buy button"
    Then user view multiple "film buy" channels

  Scenario: View Film Page - WIKI
    Scenario_label=Film page_WIKI_US
    When user clicks on "film submenu"
    Then "film page" is displayed
    When user clicks on "some film"
    Then "WIKI option" is displayed

  Scenario: View Film Page - IMDB
    Scenario_label=Film page_IMDB_US
    When user clicks on "film submenu"
    Then "film page" is displayed
    When user clicks on "some film"
    Then "IMDB option" is displayed

  Scenario: View Video Page - watch video
    Scenario_label=StarWars_US_VideoMenu
    When user clicks on "video submenu"
    Then "video page" is displayed
    And user clicks on "play icon" to play the video
    And user select the "video" from the playlist
    Then user navigates to the "watch video" page of the video

  Scenario: View TV-Shows Page - select article
    Scenario_label=StarWars_US_TV-ShowsMenu
    When user clicks on "TV-shows submenu"
    Then "TV-shows page" is displayed
    And user clicks on the "series article"
    Then "series article" page should get displayed

  Scenario: View Games Page - select article
    Scenario_label=StarWars_US_GamesMenu
    When user clicks on "Games submenu"
    Then "Games page" is displayed
    And user clicks on the "Interactive article"
    Then "Interactive article" page should get displayed

  Scenario: View Community Page - select article
    Scenario_label=StarWars_US_CommunityMenu
    When user clicks on "community submenu"
    Then "community page" is displayed
    And user clicks on the "community article"
    Then "community article" page should get displayed

  Scenario: View Databank Page - select article
    Scenario_label=StarWars_US_DatabankMenu
    When user clicks on "databank submenu"
    Then "databank page" is displayed
    And user clicks on the "databank article"
    Then "databank article" page should get displayed

  Scenario: View StarWars 404 Page - error page
    Scenario_label=StarWars_US_404Page
    When user navigated to "foo" page
    Then "error page" is displayed

  Scenario: View StarWars 500 Page - internal server error page
    Scenario_label=StarWars_US_500Page
    When user navigated to "500.html" page
    Then "error page" is displayed

  Scenario: Social link "Facebook" navigation
    Scenario_label=Navigation_Facebook_US
    When user clicks on "Facebook"
    Then user navigates to "Facebook" page

  Scenario: Social link "Tumblr" navigation
    Scenario_label=Navigation_Tumblr_US
    When user clicks on "Tumblr"
    Then user navigates to "Tumblr" page

  Scenario: Social link "Twitter" navigation
    Scenario_label=Navigation_Twitter_US
    When user clicks on "Twitter"
    Then user navigates to "Twitter" page

  Scenario: Social link "Instagram" navigation
    Scenario_label=Navigation_Instagram_US
    When user clicks on "Instagram"
    Then user navigates to "Instagram" page

  Scenario: Social link "Youtube" navigation
    Scenario_label=Navigation_Youtube_US
    When user clicks on "Youtube"
    Then user navigates to "Youtube" page

  Scenario: Footer link "Terms of Use" navigation
    Scenario_label=Navigation_Terms of Use_US
    When user clicks on "Terms of Use" from "Footer" list
    Then user navigates to "Terms of Use" page

  Scenario: Footer link "Legal Notices" navigation
    Scenario_label=Navigation_Legal Notices_US
    When user clicks on "Legal Notices" from "Footer" list
    Then user navigates to "Legal Notices" page

  Scenario: Footer link "Privacy Policy" navigation
    Scenario_label=Navigation_Privacy Policy_US
    When user clicks on "Privacy Policy" from "Footer" list
    Then user navigates to "Privacy Policy" page

  Scenario: Footer link "Childrens Online Privacy Policy" navigation
    Scenario_label=Navigation_Childrens Online Privacy Policy_US
    When user clicks on "Childrens Privacy Policy" from "Footer" list
    Then user navigates to "Childrens Privacy Policy" page

  Scenario: Footer link "Your California Privacy Rights" navigation
    Scenario_label=Navigation_Your California Privacy Rights_US
    When user clicks on "Your California Privacy Rights" from "Footer" list
    Then user navigates to "Your California Privacy Rights" page

  #Scenario: Footer link "Star Wars at shopDisney" navigation
  #Scenario_label=Navigation_Star Wars at shopDisney_US
  #When user clicks on "Star Wars at shopDisney" from "Footer" list
  #Then user navigates to "Star Wars at shopDisney" page
  
  Scenario: Footer link "Star Wars Helpdesk" navigation
    Scenario_label=Navigation_Star Wars Helpdesk_US
    When user clicks on "Star Wars Helpdesk" from "Footer" list
    Then user navigates to "Star Wars Helpdesk" page

  Scenario: Footer link "Interest-Based Ads" navigation
    Scenario_label=Navigation_Interest-Based Ads_US
    When user clicks on "Interest-Based Ads" from "Footer" list
    Then user navigates to "Interest-Based Ads" page
