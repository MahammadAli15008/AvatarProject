@starwars @starwars-video
Feature: Starwars Regressions - Audio and Video

  Background: 
    Given the user is on the Disney "starwars_us" page

  Scenario: Perform Video Operations on Video - Play and Pause
  Scenario_label=Video_Play and Pause
    When user navigates to the "video" page
    And the video player is loaded
    And the user clicks on the Video pause button
    Then the video should be paused
    And the user clicks on the Video play button
    Then the video should be resume to play

  Scenario: Perform Video Operations on Video - Fullscreen and Exitscreen
  Scenario_label=Video_Fullscreen and Exit
    When user navigates to the "video" page
    And the video player is loaded
    And the user clicks on the Video fullscreen button
    Then the video should be displayed in fullscreen
    And the user clicks on the Video collapse fullscreen button
    Then the video should be displayed in normal screen

  Scenario: Perform Video Operations on Video - Mute and Unmute
  Scenario_label=Video_mute and Unmute
    When user navigates to the "video" page
    And the video player is loaded
    And the user clicks on the Video mute button
    Then the video should be mute
    And the user clicks on the Video unmute button
    Then the video should be unmute

  Scenario: Perform Video Operations on Video - Captions ON
  Scenario_label=Video_Captions ON
    When user navigates to the "video" page
    And the video player is loaded
    And the user clicks on the Video captions "ON" button
    Then the video should displayed with Captions

  Scenario: Perform Video Operations on Video - Captions OFF and dragScrubber
  Scenario_label=Video_Captions OFF and Drag Scrubber
    When user navigates to the "video" page
    And the video player is loaded
    And the user clicks on the Video captions "OFF" button
    Then the video should displayed without Captions
    And the user drags the Video player progress slider to the left
    Then the video should be resume to play

  Scenario: Perform Audio Operations on Audio - Play
  Scenario_label=Audio_play
    When user navigates to the "audio" page
    And the audio player is loaded
    Then the audio should be played
