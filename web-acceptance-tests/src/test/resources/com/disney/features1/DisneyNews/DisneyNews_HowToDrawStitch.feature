@disneyNews @drawStitch_Ada 
Feature: ADA & Page Content - Draw Stitch With Walt Disney Animation Studios! 

Background: 
	Given the user is on the "howtodrawstitch_disneynews" page 
	
Scenario: Verify all the images should contain 'alt or aria-label' tag 
	Then all the images should have alt or aria-label tag 
	
Scenario: Verify the contents of 'Related Articles' section 
	When user click on the "2" "Related Articles" card grid 
	Then user should navigates to the selected "Related Articles" page 
	
Scenario: Verify the contents of "Inc rich article" section 
	Then user should be able to view the contents of "Inc rich article" section 
	
Scenario: Verify the contents of "Video Player" section 
	Then user should be able to view the contents of "Video Player" section 
	
Scenario: Verify the contents of "Rich text container" section 
	Then user should be able to view the contents of "Rich text container" section