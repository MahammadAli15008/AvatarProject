@disneyNewsOfficial-Ada
Feature: ADA & Page Content - Disney News The Official News Site for Disney

  Background: 
    Given the user is on the "disneynews_home" page

  @disneyNews
  Scenario: Verify all the images should contain 'alt or aria-label' tag
    Then all the images should have alt or aria-label tag

  @disneyNews-promo
  Scenario: Page content 'Promo banner' redirection
    When user click on the "Promo banner" "active" button
    Then user should redirect to the Disney "Magic Moments" page

  @disneyNews
  Scenario: Page content 'Article Page' card grid
    When user click on the "2" "ArticlePage" card grid
    Then user should navigates to the selected "ArticlePage" page

  @disneyNews
  Scenario: Page content 'article' Headlines
    When user click on the "1" "ArticleHeadlines" card grid
    Then user should navigates to the selected "ArticleHeadlines" page

  @disneyNews
  Scenario: Navigate to all the page content article pages
    When user get the list of "page content" articles from the grid
    Then user should navigates to the each "page content" article pages
