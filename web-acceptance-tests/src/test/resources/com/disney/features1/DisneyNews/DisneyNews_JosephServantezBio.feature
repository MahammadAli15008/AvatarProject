@disneyNews @authorJosephBio_Ada 
Feature: ADA & Page Content - Joseph Servantez Author Bio 
 
Background: 
	Given the user is on the "authorjosephbio_disneynews" page 
	
Scenario: Verify all the images should contain 'alt or aria-label' tag 
	Then all the images should have alt or aria-label tag 
	
Scenario: Verify the contents of 'More From this Author' section 
	When user click on the "1" "More From this Author" card grid 
	Then user should navigates to the selected "More From this Author" page 
	
Scenario: Verify the contents of "Rich text container" section 
	Then user should be able to view the contents of "Rich text container" section