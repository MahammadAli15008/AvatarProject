@disneyNews @disneyNewsOfficial-Footer 
Feature: Footer Links - Disney News The Official News Site for Disney 
 
Background: 
	Given the user is on the "disneynews_home" page 
	
Scenario: Navigate to 'Disney Guest Services' Page through footer link 
	And the user clicked on the "Contact Us" "footer links" 
	Then user should navigate to the "Disney Guest Services" page 
	
Scenario: Navigate to 'Legal Notices' Page through footer link 
	And user click on the "Additional Content Information" "footer links" 
	Then user should navigates to the "Legal Notices" page 
	
Scenario: Navigate to 'Privacy Policy' Page through footer link 
	And user click on the "Privacy Policy" "footer links" 
	Then user should navigates to the "Privacy Policy" page 
	
Scenario: Navigate to 'Your California Privacy Rights' Page through footer link 
	And user click on the "Your California Privacy Rights" "footer links" 
	Then user should navigates to the "Your California Privacy Rights" page 
	
Scenario: Navigate to 'Children's Privacy Policy' Page through footer link 
	And user click on the "Children�s Privacy Policy" "footer links" 
	Then user should navigates to the "Children�s Privacy Policy" page 
	
Scenario: Navigate to 'Interest-Based Ads' Page through footer link 
	And user click on the "Interest-Based Ads" "footer links" 
	Then user should navigates to the "Interest-Based Ads" page 
	
Scenario: Navigate to 'Do Not Sell My Info' Page through footer link 
	And user click on the "Do Not Sell My Info" "footer links" 
	Then user should navigates to the 'Do Not Sell My Info' page 
	
Scenario: Navigate to 'The Walt Disney Company' Page through footer link 
	And user click on the "About Disney" "footer links" 
	Then user should navigates to the "The Walt Disney Company" page 
	
Scenario: Navigate to 'Disney Guest Services' Page through footer link 
	And user click on the "Disney Help" "footer links" 
	Then user should navigates to the "Disney Guest Services" page 
	
Scenario: Navigate to 'Working at DISNEY' Page through footer link 
	And user click on the "Careers" "footer links" 
	Then user should navigates to the "Jobs and Careers at DISNEY" page 
	
Scenario: Navigate to 'Disney Advertising Sales' Page through footer link 
	And  user click on the "Advertise With Us" "footer links" 
	Then  user should navigates to the "Disney Advertising Sales" page 
	
Scenario: Navigate to 'Disney Premier Credit Card' Page through footer link 
	And  user click on the "Disney Premier Visa Card" "footer links" 
	Then  user should navigates to the "Disney Premier Credit Card" page