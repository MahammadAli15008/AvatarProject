@natgeo-footer
Feature: National Geographic Films - Footer Links Tests

  Background: 
    Given the user is on the "natgeo_films" page

  @natgeo
  Scenario: Navigates to 'Contact Us' Page through footer link
    And user click on the "Contact Us" "footer link"
    Then user should navigates to the "Contact Us" page

  @natgeo
  Scenario: Navigates to 'Terms of Use' Page through footer link
    And user click on the "Terms of Use" "footer link"
    Then user should navigates to the "Terms of Use" page

  @natgeo
  Scenario: Navigates to 'Privacy Policy' Page through footer link
    And user click on the "Privacy Policy" "footer link"
    Then user should navigates to the "Privacy Policy" page

  @natgeo
  Scenario: Navigates to 'Your California Privacy Rights' Page through footer link
    And user click on the "Your California Privacy Rights" "footer link"
    Then user should navigates to the "Your California Privacy Rights" page

  @natgeo
  Scenario: Navigates to 'Children�s Privacy Policy' Page through footer link
    And user click on the "Children�s Privacy Policy" "footer link"
    Then user should navigates to the "Children�s Privacy Policy" page

  @natgeo
  Scenario: Navigates to 'Interest-Based Ads' Page through footer link
    And user click on the "Interest-Based Ads" "footer link"
    Then user should navigates to the "Interest-Based Ads" page

  @natgeo
  Scenario: Navigates to 'Do Not Sell My Info' Page through footer link
    And user click on the "Do Not Sell My Info" "footer link"
    Then user should navigates to the "Do Not Sell My Info" page

  @natgeo
  Scenario: Navigates to 'Facebook' Page through footer link
    And user click on the "Facebook" "footer link"
    Then user should navigates to the "Facebook" page

  @natgeo
  Scenario: Navigates to 'Twitter' Page through footer link
    And user click on the "Twitter" "footer link"
    Then user should navigates to the "Twitter" page

  @natgeo
  Scenario: Navigates to 'Instagram' Page through footer link
    And user click on the "Instagram" "footer link"
    Then user should navigates to the "Instagram" page
