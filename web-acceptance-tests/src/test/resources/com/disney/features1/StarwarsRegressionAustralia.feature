@starwars @starwars-aus
Feature: Starwars Regression checks - Australia

  Background: 
    Given the user is on the Disney "starwars_aus" page

  Scenario: Footer link "Standards & Practices" navigation
    Scenario_label=Navigation_Standards & Practices_Aus
    When user clicks on "Standards & Practices" from "Footer" list
    Then user navigates to "Standards & Practices" page

  Scenario: Footer link "Help & Guest Services" navigation
    Scenario_label=Navigation_Help & Guest Services_Aus
    When user clicks on "Help & Guest Services" from "Footer" list
    Then user navigates to "Help & Guest Services" page

  Scenario: Footer link "Careers" navigation
    Scenario_label=Navigation_Careers_Aus
    When user clicks on "Careers" from "Footer" list
    Then user navigates to "Careers" page

  Scenario: Footer link "Internet Safety" navigation
    Scenario_label=Navigation_Internet Safety_Aus
    When user clicks on "Internet Safety" from "Footer" list
    Then user navigates to "Internet Safety" page

  Scenario: Footer link "Terms of Use" navigation
    Scenario_label=Navigation_Terms of Use_Aus
    When user clicks on "Terms of Use" from "Footer" list
    Then user navigates to "Terms of Use" page

  Scenario: Footer link "Privacy Policy" navigation
    Scenario_label=Navigation_Privacy Policy_Aus
    When user clicks on "Privacy Policy" from "Footer" list
    Then user navigates to "Privacy Policy" page

  Scenario: Footer link "Star Wars Helpdesk" navigation
    Scenario_label=Navigation_Star Wars Helpdesk_Aus
    When user clicks on "Star Wars Helpdesk" from "Footer" list
    Then user navigates to "Star Wars Helpdesk" page

  Scenario: View Starwars Home page
    Scenario_label=Starwars.com page_Search bar_Aus
    Then "Starwars icon" is displayed
    And "Search bar" is displayed

  Scenario: View News Page - categories
    Scenario_label=News page_Categories_Aus
    When user clicks on "news submenu"
    Then "news page" is displayed
    And "news categories" are displayed

  #Scenario: View Events Page - categories
  #Scenario_label=Events page_Categories_Aus
  #When user clicks on "event submenu"
  #Then "event page" is displayed
  #And "event categories" are displayed
  
  Scenario: View Film Page - Synopsis
    Scenario_label=Film page_Synopsis_Aus
    When user clicks on "film submenu"
    Then "film page" is displayed
    When user clicks on "some film"
    Then "film synopsis" is displayed

  Scenario: View Video Page - watch video
    Scenario_label=StarWars_Aus_VideoMenu
    When user clicks on "video submenu"
    Then "video page" is displayed
    And user clicks on "play icon" to play the video
    And user select the "video" from the playlist
    Then user navigates to the "watch video" page of the video

  Scenario: View TV-Shows Page - select article
    Scenario_label=StarWars_Aus_TV-ShowsMenu
    When user clicks on "TV-shows submenu"
    Then "TV-shows page" is displayed
    And user clicks on the "series article"
    Then "series article" page should get displayed

  Scenario: View Games Page - select article
    Scenario_label=StarWars_Aus_GamesMenu
    When user clicks on "Games submenu"
    Then "Games page" is displayed
    And user clicks on the "Interactive article"
    Then "Interactive article" page should get displayed

  Scenario: View Community Page - select article
    Scenario_label=StarWars_Aus_CommunityMenu
    When user clicks on "community submenu"
    Then "community page" is displayed
    And user clicks on the "community article"
    Then "community article" page should get displayed

  Scenario: View Databank Page - select article
    Scenario_label=StarWars_Aus_DatabankMenu
    When user clicks on "databank submenu"
    Then "databank page" is displayed
    And user clicks on the "databank article"
    Then "databank article" page should get displayed
