@20thcentury @20thcenturyhome-navigation
Feature: 20th Century Studios_Home - Navigation Tests

  Background: 
    Given the user is on the "20thcentury_home" page

  Scenario: View 20th Century Studios_Official Site page
    Then "20thcenturystudios" icon is displayed

  Scenario: Navigates to  20thcenturystudios.com through logo
    And user click on the "20thcenturystudios" "logo"
    Then user should redirect to the "20th Century Studios | Official Site" page

  Scenario: Navigates to 20thcenturystudios Page through Home tab
    And user mouse over on the "more" "tab"
    And user click on the "20thcentury Home" "tab"
    Then user should redirect to the "20th Century Studios | Official Site" page

  Scenario: Navigates to 20thcenturystudios Page through Movies tab
    And user mouse over on the "more" "tab"
    And user click on the "20thcentury Movies" "tab"
    Then user should redirect to the "Movies | 20th Century Studios" page
