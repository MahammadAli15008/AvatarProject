@20thcentury @20thcenturyfamily @20thcenturyfamilymovies-navigation
Feature: 20th Century Studios Family_Movies - Navigation Tests

  Background: 
    Given the user is on the "thecallofthewild" page

  Scenario: View "The Call of the Wild | 20th Century Studios Family" page
    Then "20thcenturystudios" icon is displayed

  Scenario: Navigates to  20th Century Studios Family page through logo
    And user click on the "20thcenturystudios" "logo"
    Then user should redirect to the "20th Century Studios Family | Home" page

  Scenario: Navigates to 20th Century Studios Family Page through Home tab
    And user mouse over on the "more" "tab"
    And user click on the "20thcentury Home" "tab"
    Then user should redirect to the "20th Century Studios Family | Home" page

  Scenario: Navigates to 20th Century Studios Family  Page through Movies tab
    And user mouse over on the "more" "tab"
    And user click on the "20thcentury Movies" "tab"
    Then user should redirect to the "Movies | 20th Century Studios Family" page
