@20thcentury @20thcenturyfamily @20thcenturyfamilymovies-ada 
Feature: 20th Century Studios Family_Movies - ADA & Page Content Tests 

Background: 
	Given the user is on the "thecallofthewild" page 
	
Scenario: Verify all the images should contain 'alt or aria-label' tag. 
	Then all the images should have alt or aria-label tag 
	
Scenario: Featured Content Banner section 
	Then user should be able to view page contents of "The Call of the Wild" 
	
Scenario: Page content 'IMAGE GALLERY' slider navigation 
	And user click on the "1" "IMAGE GALLERY" on the tiles 
	Then user should navigates to the "1" "Selected" page 
	
Scenario: Page content 'RECOMMENDED MOVIES' slider navigation 
	And user click on the "1" "RECOMMENDED MOVIES" on the tiles 
	Then user should navigates to the "1" "Selected" page 
	
Scenario: Page content 'VIDEOS' slider navigation 
	And user click on the "1" "VIDEOS" on the tiles 
	Then user should navigates to the "1" "Selected" page 