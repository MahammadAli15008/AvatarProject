@20thcentury @20thcenturymovies-ada
Feature: 20th Century Studios_Movies - ADA & Page Content Tests

  Background: 
    Given the user is on the "thenewmutants" page

  Scenario: Verify all the images should contain 'alt or aria-label' tag.
    Then all the images should have alt or aria-label tag

  Scenario: Featured Content Banner section
    Then user should be able to view page contents of "The New Mutants"

  Scenario: Page content 'IMAGE GALLERY' slider navigation
    And user click on the "1" "IMAGE GALLERY" on the tiles
    Then user should navigates to the "1" "Selected" page

  Scenario: Page content 'RECOMMENDED MOVIES' slider navigation
    And user click on the "1" "RECOMMENDED MOVIES" on the tiles
    Then user should navigates to the "1" "Selected" page

  Scenario: Page content 'VIDEOS' slider navigation
    And user click on the "1" "VIDEOS" on the tiles
    Then user should navigates to the "1" "Selected" page
