@20thcentury @20thcenturyfamily @20thcenturyfamilyhome-footer
Feature: 20th Century Studios Family_Home - Footer Links Tests

  Background: 
    Given the user is on the "20thcenturyfamily_home" page

  Scenario: Navigates to 'Contact Us' Page through footer link
    And the user clicked on the "Contact Us" "footer links"
    Then user should navigate to the "Contact Us" page

  Scenario: Navigates to 'Terms of Use' Page through footer link
    And user click on the "Terms of Use" "footer links"
    Then user should navigates to the "Terms of Use" page

  Scenario: Navigates to 'Privacy Policy' Page through footer link
    And user click on the "Privacy Policy" "footer links"
    Then user should navigates to the "Privacy Policy" page

  Scenario: Navigates to 'Your California Privacy Rights' Page through footer link
    And user click on the "Your California Privacy Rights" "footer links"
    Then user should navigates to the "Your California Privacy Rights" page

  Scenario: Navigates to 'Children's Privacy Policy' Page through footer link
    And user click on the "Children�s Privacy Policy" "footer links"
    Then user should navigates to the "Children�s Privacy Policy" page

  Scenario: Navigates to 'Interest-Based Ads' Page through footer link
    And user click on the "Interest-Based Ads" "footer links"
    Then user should navigates to the "Interest-Based Ads" page

  Scenario: Navigates to 'Do Not Sell My Info' Page through footer link
    And user click on the "Do Not Sell My Info" "footer links"
    Then user should navigates to the 'Do Not Sell My Info' page

  #Commenting  as suggested from qa
  #Scenario: Navigates to 'Blue Sky' Page through footer link
  #And user click on the "Blue Sky" "footer links"
  #Then user should navigates to the "Blue Sky" page
  
  Scenario: Navigates to 'Searchlight' Page through footer link
    And user click on the "Searchlight" "footer links"
    Then user should navigates to the "Searchlight" page

  Scenario: Navigates to 'Redeem Digital Codes' Page through footer link
    And user click on the "Redeem Digital Codes" "footer links"
    Then user should navigates to the "Fox Digital Redemption" page
