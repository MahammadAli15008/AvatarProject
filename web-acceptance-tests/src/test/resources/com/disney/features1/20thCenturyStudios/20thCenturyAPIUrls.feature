@20thcenturyFrontEndUrls
Feature: 20th Century Front End URL's Status Check 

@20thcentury @20thcentury_api_qa
Scenario: Verify status 200 on 20th Century FE URL's
	When the user hits a collection of 20th Century FE Urls 
	Then list of 20th century FE Urls status should be 200 
	
@20thcentury_api_prod 
Scenario: Verify status 301 on 20th Century FE URL's
	When the user hits a collection of 20th Century FE Urls
	Then list of 20th century FE Urls status should be 301