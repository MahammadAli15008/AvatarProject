@20thcentury @20thcenturymovies-navigation
Feature: 20th Century Studios_Movies - Navigation Tests

  Background: 
    Given the user is on the "thenewmutants" page

  Scenario: View 20th Century Studios_Official Site page
    Then "20thcenturystudios" icon is displayed

  Scenario: Navigates to  20th Century Studios_Official Site page
    And user click on the "20thcenturystudios" "logo"
    Then user should redirect to the "20th Century Studios | Official Site" page

  Scenario: Navigates to 20th Century Studios Page through Home tab
    And user mouse over on the "more" "tab"
    And user click on the "20thcentury Home" "tab"
    Then user should redirect to the "20th Century Studios | Official Site" page

  Scenario: Navigates to 20th Century Studios Page through Movies tab
    And user mouse over on the "more" "tab"
    And user click on the "20thcentury Movies" "tab"
    Then user should redirect to the "Movies | 20th Century Studios" page
