@20thcentury @20thcenturyhome-ada
Feature: 20th Century Studios_Home - ADA & Page Content Tests

  Background: 
    Given the user is on the "20thcentury_home" page

  Scenario: Verify all the images should contain 'alt or aria-label' tag.
    Then all the images should have alt or aria-label tag

  Scenario: Page content 'Promo banner' Left navigation
    And user click on the "Promo banner" "left arrow" button
    Then user should navigates to the "Promo banner" "Next" slide

  Scenario: Page content 'Promo banner' Right navigation
    And user click on the "Promo banner" "right arrow" button
    Then user should navigates to the "Promo banner" "Previous" slide

  Scenario: Page content 'Movie Poster' slider navigation
    And user click on the "1" "Movie" on the Movie Poster
    Then user should navigates to the "1" "Movie" page

  Scenario: Page content 'Featured Titles' slider navigation
    And user click on the "1" "Featured Titles" on the tiles
    Then user should navigates to the "1" "Selected" page

  Scenario: Page content 'ON BLU-RAY, DVD & DIGITAL' slider navigation
    And user click on the "1" "On BLU-RAY" on the tiles
    Then user should navigates to the "1" "Selected" page

  Scenario: Page content 'VIDEOS' slider navigation
    And user click on the "1" "VIDEOS" on the tiles
    Then user should navigates to the "1" "Selected" page
