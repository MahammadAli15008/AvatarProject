@TwoWordAsOneSearch @SearchAdminRegression
Feature: Two word as One word Search

  Scenario: Search results for 'BuzzLightyear'
    Given the user is on Disney Search page
    When the user performs search on the word "BuzzLightyear"
    Then the word should not be duplicate in 'results found' and 'did you mean'
