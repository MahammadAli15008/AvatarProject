@DisableSpellCheck @SearchAdminRegression
Feature: Disablespellcheck = true

  Scenario: Results when disablespellcheck is true
    Given the user is on DisneyUS search url having spell check disabled
    When the user performs search with incorrect keyword "mickky"
    Then the response should suggest correct keyword "mickey"
