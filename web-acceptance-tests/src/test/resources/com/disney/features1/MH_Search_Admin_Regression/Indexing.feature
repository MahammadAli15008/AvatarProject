@Indexing @SearchAdminRegression
Feature: Indexing collection data

  Background: 
    Given the user is on the Search "adminhome" page
    And the collection is already created

  Scenario: Indexing the Collection with Custom Sources
    When the user navigates to the custom sources url
    And the user indexes the collection with custom data source
    Then indexing with "customsources" should be successful

  Scenario: Search results after Indexing Custom Sources
    When the user performs "search" for "Mater"
    Then response should contain some search results for "Mater"

  Scenario: Autocomplete search results after Indexing Custom Sources
    When the user performs "autocompletesearch" for "Mater"
    Then response should contain some search results for "Mater"

  Scenario: Indexing the Collection with Data sources
    When the user navigates to the datasources tab
    And the user indexes the collection with data sources
    Then indexing with "datasources" should be successful

  Scenario: Indexing the Collection with Index sources
    When the user navigates to the index sources url
    And the user selects the index option for the cms url
    Then indexing with "indexsources" should be successful

  Scenario: Search results after Indexing Data Sources
    When the user performs "search" for "Mater"
    Then response should contain some search results for "Mater"

  Scenario: Autocomplete search results after Indexing Data Sources
    When the user performs "autocompletesearch" for "Mater"
    Then response should contain some search results for "Mater"
