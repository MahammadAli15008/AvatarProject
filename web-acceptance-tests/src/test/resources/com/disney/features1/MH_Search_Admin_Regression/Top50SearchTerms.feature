@SearchAdminProd
Feature: DisneyUS - Top 50 Search Terms

  @Top50SearchTerms
  Scenario: Perform search for all the top 50 search terms
    Given the user navigates to the Disney US Search page
    When the user performs search for each search term
    Then the respective search results page should be displayed

  @404LinkCheck
  Scenario: No Bad/Broken links on the search term results pages
    Given the user is on search results page
    When the user performs link check for each search results page
    Then search results page should not contain any broken links
