@CollectionAdmin @SearchAdminRegression
Feature: Collection admin page

  Background: 
    Given the user is on the Search "adminhome" page

  Scenario: Collection View Tab for Disney US collection
    When user is on "view" tab for "disney us" collection
    Then content should populate in View tab

  Scenario: Collection Edit Tab for Disney US collection
    When user is on "edit" tab for "disney us" collection
    Then content should populate in Edit tab

  Scenario: Collection Stats Tab for Disney US collection
    When user is on "stats" tab for "disney us" collection
    Then content should populate in Stats tab

  Scenario: Collection Search Metrics Tab for Disney US collection
    When user is on "search metrics" tab for "disney us" collection
    Then content should populate in Search Metrics tab
