@AddCollection @SearchAdminRegression
Feature: MH Search Add Collection page

  Background: 
    Given the user is on the Search "adminhome" page

  Scenario: Add and search for the New Collection
    When the user navigates to the New Collection Admin page
    And the user creates a new collection
    Then a new collection should be created
