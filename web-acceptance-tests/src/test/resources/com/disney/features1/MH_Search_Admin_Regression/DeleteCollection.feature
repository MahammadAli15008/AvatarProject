@DeleteCollection
Feature: MH Search - Delete collection

  Background: 
    Given the user is on the Search "adminhome" page

  Scenario: Delete collection
    When the user is on the Collection Admin page
    And the user deletes the collection
    Then the collection should be no longer available
