@BabbleEndpoint @SearchAdminRegression
Feature: Babble collection - Search Results

  Scenario: Perform search by keyword
    Given the MH Search endpoint
    When the user perform "search" with keyword "baby"
    Then the results returned should contain the search key "baby"

  Scenario: Perform autocomplete search by keyword
    Given the MH Search endpoint
    When the user perform "autocompleteSearch" with keyword "baby"
    Then the results returned should contain the search key "baby"
