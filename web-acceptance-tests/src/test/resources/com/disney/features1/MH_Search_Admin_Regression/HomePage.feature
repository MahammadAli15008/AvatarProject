@HomePage @SearchAdminRegression
Feature: MH Search Admin Home page

  Background: 
    Given the user is on the Search "adminhome" page

  Scenario: Collection search for Disney US
    When user searches for "disney us" collection dropdown
    And user clicks on the search results
    Then user should be redirected to admin page for "disney us" collection

  Scenario: Collection search for Disn
    When user searches for "disn" collection dropdown
    Then the auto complete results should contain "disn"
