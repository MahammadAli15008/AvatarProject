@natgeo-navigation
Feature: National Geographic Films - Navigation Tests

  Background: 
    Given the user is on the "natgeo_films" page

  @natgeo
  Scenario: View NatGeo.com page
    Then "NationalGeographic" icon is displayed

  @natgeo
  Scenario: Navigates to Natgeo Page through logo
    And user click on the "National Geographic" "logo"
    Then user should redirects to the "National Geographic" page

  @natgeo
  Scenario: Navigates to Natgeo Page through Home tab
    And user click on the "NatGeo Home" "tab"
    Then user should redirects to the "National Geographic" page

  @natgeo
  Scenario: Navigates to 'all Disney+' Pages through Disney+ tab
    When user mouse over on the "NatGeo Disney+" "tab"
    Then user should redirects to all the "NatGeo Disney+" "option" options
    
  @natgeo
  Scenario: Navigates to 'New NatGeo Release' Pages through Disney+ tab
    And user mouse over on the "NatGeo NewReleases" "tab"
    Then user should redirects to all the "NatGeo NewReleases" "option" options