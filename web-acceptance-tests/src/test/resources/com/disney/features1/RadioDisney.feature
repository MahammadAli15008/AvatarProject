@RadioDisney
Feature: 039 - Radio Disney Content Loads

  Background: 
    Given the user is on the Disney "radiodisney" page

  @EmbeddedPlayer
  Scenario: User selects a different station
    Scenario_label=DifferentStation
    When user clicks "popup"
    And user is "on" radiodisney "popup element" window
    And user clicks "different station"
    Then "different station" is present

  @EmbeddedPlayer
  Scenario: User can close the Radio Disney station popup window
    Scenario_label=CloseRadioDisneyPopup
    When user clicks "popup"
    And user is "on" radiodisney "popup element" window
    And user close the "popup window"
    Then user verify the "popup" window is closed

  @PopOutPlayer
  Scenario: Changing radio station on the Radio Disney pop-out player
    Scenario_label=ChangeRadioStation
    When user clicks "popout"
    And user is "on" radiodisney "popout player" window
    And user clicks "different station"
    Then "verify station" is present

  @PopOutPlayer
  Scenario: Pause/play Radio Disney player
    Scenario_label=PausePlayRadioPlayer
    When "play pause button" is present
    And user clicks "play pause button"
    Then "play pause button" is played

  @PopOutPlayer
  Scenario: User can close the Radio Disney pop-out player
    Scenario_label=CloseRadioPlayer
    When user clicks "popout"
    And user is "on" radiodisney "popout player" window
    And user close the "popout player"
    Then user verify the "popout player" window is closed
