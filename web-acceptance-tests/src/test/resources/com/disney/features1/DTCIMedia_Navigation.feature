@dtci-navigation
Feature: DTCI Media - Navigation Tests

  Background: 
    Given the user is on the "dtci_media" page

  @dtci
  Scenario: View DTCIMedia.com page
    Then "DTCIMedia" icon is displayed

  @dtci
  Scenario: Navigates to DTCI Media Page through logo
    When user clicks on "DTCIMedia" "logo"
    Then user should redirects to "DTCI Media" page

  @dtci
  Scenario: Navigates to DTCI About Page through About tab
    When user clicks on "DTCI About" "tab"
    Then user should redirects to "DTCI About" page

  @dtci
  Scenario: Navigates to DTCI News Page through News tab
    When user clicks on "DTCI News" "tab"
    Then user should redirects to "DTCI News" page

  @dtci
  Scenario: Navigates to DTCI Disney+ Page through Disney+ tab
    When user clicks on "DTCI Disney+" "tab"
    Then user should redirects to "DTCI Disney+" page

  @dtci
  Scenario: Navigates to DTCI ESPN+ Page through ESPN+ tab
    When user clicks on "DTCI ESPN+" "tab"
    Then user should redirects to "DTCI ESPN+" page

  @dtci
  Scenario: Navigates to DTCI Hulu Page through Hulu tab
    When user clicks on "DTCI Hulu" "tab"
    Then user should redirects to "DTCI Hulu" page

  @dtci
  Scenario: Navigates to all the DTCI About dropdown options
    When user mouse over on "DTCI About" "tab"
    Then user should redirects to all "DTCI About" "dropdown" options

  @dtci
  Scenario: Navigates to all the DTCI Disney+ dropdown options
    When user mouse over on "DTCI Disney+" "tab"
    Then user should redirects to all "DTCI Disney+" "dropdown" options

  @dtci
  Scenario: Navigates to all the DTCI ESPN+ dropdown options
    When user mouse over on "DTCI ESPN+" "tab"
    Then user should redirects to all "DTCI ESPN+" "dropdown" options

  @dtci
  Scenario: Navigates to all the DTCI Footer List options
    When user move to the "DTCI Footer" "list"
    Then user should redirects to all "DTCI Footer" "list" options

  @dtci
  Scenario: Verify images should contain 'alt or aria-label' tag in 'DTCI Media' page.
    Then all the images should have alt or aria-label tag

  @dtci
  Scenario Outline: Verify images should contain 'alt or aria-label' tag in mentioned url pages.
    When the user navigates to the "<url>"
    Then the images should have alt or aria-label tag

    Examples: 
      | url                                                                                                            |
      | https://dtcimedia.disney.com/news/disney-plus-original-series-rogue-trip                                       |
      | https://dtcimedia.disney.com/news/mulan-song-loyal-brave-true-by-christina-aguilera-available-today            |
      | https://dtcimedia.disney.com/news/the-lego-star-wars-holiday-special-premieres-november-17-2020-on-disney-plus |
      | https://dtcimedia.disney.com/news/the-proud-family-cast-and-creative-team-reunited-for-naacps-arts-festival    |
      | https://dtcimedia.disney.com/news/magic-of-disneys-animal-kingdom-premiering-september-25-2020                 |

  @dtci
  Scenario: Navigates to DTCI Search Page through Search
    When user clicks on "DTCI Search" icon
    And user enters the search text "disney" and clicks on "submit" button
    Then user should redirects to search "results" page

  @dtci
  Scenario: Navigates to DTCI Search results Page through results
    When user clicks on "DTCI Search" icon
    And user enters the search text "disney" and clicks on "submit" button
    Then user should redirects to search "results" page
    And user clicks on "DTCI Search" 1 "link"
    Then user should redirects to 1 link "results" page

  @dtci
  Scenario: Navigates to DTCI Search Page through ENTER key
    When user clicks on "DTCI Search" icon
    And user enters the search text "disney" and clicks on "ENTER Key" button
    Then user should redirects to search "results" page

  @dtci
  Scenario: Navigates to DTCI Search results Page with 'Show More' option
    When user clicks on "DTCI Search" icon
    And user enters the search text "disney" and clicks on "submit" button
    Then user should redirects to search "results" page
    And user should clicks on "Show More" button
