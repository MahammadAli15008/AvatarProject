@starwars @starwars-embed
Feature: Starwars Regressions - Embed Video

  Background: 
    Given the user is on the Disney "starwars_us" page

  Scenario: Perform Embed Video Operations on Video - Play and Pause
  Scenario_label=EmbedVideo_Play and Pause
    When user navigates to the "embed video" page
    And the video player is loaded
    And the user clicks on the Video pause button
    Then the video should be paused
    And the user clicks on the Video play button
    Then the video should be resume to play

  Scenario: Perform Embed Video Operations on Video - Fullscreen and Exitscreen
  Scenario_label=EmbedVideo_Fullscreen and Exit
    When user navigates to the "embed video" page
    And the video player is loaded
    And the user clicks on the Video fullscreen button
    Then the video should be displayed in fullscreen
    And the user clicks on the Video collapse fullscreen button
    Then the video should be displayed in normal screen

  Scenario: Perform Embed Video Operations on Video - Mute and Unmute
  Scenario_label=EmbedVideo_Mute and Unmute
    When user navigates to the "embed video" page
    And the video player is loaded
    And the user clicks on the Video mute button
    Then the video should be mute
    And the user clicks on the Video unmute button
    Then the video should be unmute

  Scenario: Perform Embed Video Operations on Video - Captions ON
  Scenario_label=EmbedVideo_CaptionsON
    When user navigates to the "embed video" page
    And the video player is loaded
    And the user clicks on the Video captions "ON" button
    Then the video should displayed with Captions

  Scenario: Perform Embed Video Operations on Video - Captions OFF and dragScrubber
  Scenario_label=EmbedVideo_CaptionsOFF and DragScrubber
    When user navigates to the "embed video" page
    And the video player is loaded
    And the user clicks on the Video captions "OFF" button
    Then the video should displayed without Captions
    And the user drags the Video player progress slider to the left
    Then the video should be resume to play