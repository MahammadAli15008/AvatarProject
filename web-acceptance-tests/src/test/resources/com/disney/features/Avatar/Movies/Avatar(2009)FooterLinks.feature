Feature: Avatar - Avatar (2009) footer links

 Background: 
    Given user navigates to the "Avatar_Home" page
    When user mouse over on the "Movies section" "tab"
    When user clicks on "Avatar (2009)" "option"
    
    Scenario: Verify that all footer links are visible in the 'All Movies' page 
        When user conforms that "Terms of Use" "footer link" is visible
        When user conforms that "Privacy Policy" "footer link" is visible
        When user conforms that "Childrens online Privacy Policy" "footer link" is visible
        When user conforms that "Your California Privacy Rights" "footer link" is visible
        When user conforms that "Avatar at shopDisney" "footer link" is visible
        When user conforms that "Interest-Based Ads" "footer link" is visible
        When user conforms that "Do Not Sell My Personal info" "footer link" is visible
        When user conforms that "Facebook" "footer link" is visible
        When user conforms that "Twitter" "footer link" is visible
        When user conforms that "Instagram" "footer link" is visible
        When user conforms that "Youtube" "footer link" is visible