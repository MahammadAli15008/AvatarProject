Feature: Avatar - Community Avatar Section - Audio

Background: 
    Given user navigates to the "Avatar_Home" page
    When user clicks on "Tab" "Community"
    When user moves to the "Audio Section" "module"
    
    Scenario: Verify taht Click the play button initiates a audio play back
        When user Click the play button initiates a audio play back in "Audio Section"
