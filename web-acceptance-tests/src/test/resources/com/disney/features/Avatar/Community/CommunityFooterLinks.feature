Feature: Avatar - Community Avatar Section - Footer Links

Background: 
    Given user navigates to the "Avatar_Home" page
    When user clicks on "Tab" "Community"
    
     Scenario: Verify that all footer links are visible in the 'Community' page 
        When user conforms that "Terms of Use" "footer link" is visible
        When user conforms that "Privacy Policy" "footer link" is visible
        When user conforms that "Childrens online Privacy Policy" "footer link" is visible
        When user conforms that "Your California Privacy Rights" "footer link" is visible
        When user conforms that "Avatar at shopDisney" "footer link" is visible
        When user conforms that "Interest-Based Ads" "footer link" is visible
        When user conforms that "Do Not Sell My Personal info" "footer link" is visible
        When user conforms that "Facebook" "footer link" is visible
        When user conforms that "Twitter" "footer link" is visible
        When user conforms that "Instagram" "footer link" is visible
        When user conforms that "Youtube" "footer link" is visible