Feature: Avatar - Footer Links Tests

  Background: 
    Given user navigates to the "Avatar_Home" page

  Scenario: Navigates to 'Terms of Use' Page through footer link
    And user click on the "Terms of Use" "footer links"
    Then user should navigates to the "Terms of Use" page

  Scenario: Navigates to 'Privacy Policy' Page through footer link
    And user click on the "Privacy Policy" "footer links"
    Then user should navigates to the "Privacy Policy" page
 
  Scenario: Navigates to 'Childrens online Privacy Policy' Page through footer link
    And user click on the "Childrens online Privacy Policy" "footer links"
    Then user should navigates to the "Childrens online Privacy Policy" page

  Scenario: Navigates to 'Your California Privacy Rights' Page through footer link
    And user click on the "Your California Privacy Rights" "footer links"
    Then user should navigates to the "Your California Privacy Rights" page
   
  Scenario: Navigates to 'Avatar at shopDisney' Page through footer link
    And user click on the "Avatar at shopDisney" "footer links"
    Then user should navigates to the "Avatar at shopDisney" page
  
  Scenario: Navigates to 'Interest-Based Ads' Page through footer link
    And user click on the "Interest-Based Ads" "footer links"
    Then user should navigates to the "Interest-Based Ads" page
   
  Scenario: Navigates to 'Do Not Sell My Personal info' Page through footer link
    And user click on the "Do Not Sell My Personal Info" "footer links"
    Then user should navigates to the "Do Not Sell my Personal Info" page
  
   Scenario: Navigates to 'Facebook' Page through footer link
    And user click on the "Facebook" "footer links"
    Then user should navigates to the "facebook" page
   
  Scenario: Navigates to 'Twitter' Page through footer link
    And user click on the "Twitter" "footer links"
    Then user should navigates to the "twitter" page
    
  Scenario: Navigates to 'Instagram' Page through footer link
    And user click on the "Instagram" "footer links"
    Then user should navigates to the "instagram" page
    
  Scenario: Navigates to 'Youtube' Page through footer link
    And user click on the "Youtube" "footer links"
    Then user should navigates to the "youtube" page
