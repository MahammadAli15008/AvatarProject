package com.disney.utilities.managers;

public class TestDataManager {

	public static final String HOMEPAGE_SEARCHADMINHEADER = "Search Admin";
	public static final String ADDCOLLECTIONPAGE_FORMLABELS = "Collection ID *,Collection Name *,Locale";
	public static final String ADDCOLLECTIONPAGE_SHARDINGLABELS = "Number Of Shards,Replication Factor,Is Collection Internal?,Solr Schema Name";
	public static final String ADDCOLLECTIONPAGE_COLLECTIONID = "QE_CollectionID";
	public static final String ADDCOLLECTIONPAGE_COLLECTIONNAME = "QE_CollectionName";
	public static final String ADDCOLLECTIONPAGE_LOCALEINPUT = "India";
	public static final String ADDCOLLECTIONPAGE_TITLE = "New Collection Admin";
	public static final String BABBLEENDPOINT_SEARCH = "babble/search.json?q=";
	public static final String BABBLEENDPOINT_AUTOCOMPLETESEARCH = "babble/search/autocomplete.json?prefix=";
	public static final String DELETECOLLECTION_NORESULTS = "NO RESULTS";
	public static final String TOP50SEARCHTERMS_SEARCHMETRICSURL = "https://search.matterhorn.disney.io/admin/collections/disneyus/topsearchterms";
	public static final String TOP50SEARCHTERMS_DISNEYUSHEADER = "Disney US";
	public static final String TOP50SEARCHTERMS_DISNEYSEARCHTITLE = "Disney Search";
	public static final String INDEXING_CUSTOMSOURCESLABELS = "CMS URLs,Feed URLs,Store Feed URLs,Site Map URLs";
	public static final String INDEXING_CMSURLFIELDDATA = "http://cms/portals/cars/character_pages/live.json?search=true";
	public static final String INDEXING_CUSTOMSOURCESEARCH = "/search.json?q=";
	public static final String INDEXING_CUSTOMSOURCESAUTOCOMPLETE = "/search/autocomplete.json?prefix=";
	public static final String INDEXING_DATASOURCES = "/indexsources";
	public static final String TWOWORDASONESEARCH_DISNEYSEARCHPAGE = "disneysearch";
	public static final String COLLECTIONADMINPAGE_VIEW_BASICDETAILSTAB = "Collection Name,Locale,Replication Factor,Internal Collection,Schema Name,Updated By,Last Modified,Search Endpoint,Autocomplete Endpoint";
	public static final String COLLECTIONADMINPAGE_VIEW_DATASOURCESTAB = "LastModified Info,CMS URLs,Feed URLs,Store Feed URLs,Site Map URLs";
	public static final String COLLECTIONADMINPAGE_VIEW_REDIRECTSTAB = "LastModified Info";
	public static final String COLLECTIONADMINPAGE_EDIT_DATASOURCESTAB = "CMS URL(s):,Feed URL(s):,Store Feed URL(s):,Site Map URL(s):";
	public static final String COLLECTIONADMINPAGE_EDIT_REDIRECTSTAB = "Redirect URLs";
	public static final String COLLETIONADMINPAGE_STATS_INDEX = "Last Index Time,Duration Since Last Index Time,Index Optimized?,Index Size,All,Video,Product,Article,Parks,Game,Movie,Portal,Gallery,Character,Show,Property,Collection,Album,Page,Homepage,video,Engine Version,Engine Start Time,Engine Up Time,API Version,API Git Revision,API Start Time,API Up Time";
	public static final String DISABLESPELLCHECK_ENDPOINT = "&disable_spellcheck=true";
	public static final String DISABLESPELLCHECK_KEYWORD1 = "did_you_mean";
	public static final String DISABLESPELLCHECK_KEYWORD2 = "corrected";
}

