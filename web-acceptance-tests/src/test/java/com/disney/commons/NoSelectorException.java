package com.disney.commons;

public class NoSelectorException extends RuntimeException
{
    public NoSelectorException(String s)
    {
        super(s);
    }
}
