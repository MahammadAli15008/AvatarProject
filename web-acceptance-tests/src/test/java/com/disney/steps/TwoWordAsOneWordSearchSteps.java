package com.disney.steps;

import static com.disney.steps.RunnerTest.tree;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.disney.common.SmartNav;
import com.disney.config.WebConfig;
import com.disney.utilities.managers.TestDataManager;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class TwoWordAsOneWordSearchSteps extends SmartNav {

	private String collectionURL = null;
	private WebConfig config = new WebConfig();

	@Given("^the user is on Disney Search page$")
	public void userIsOnDisneySearchPage() {

		try {
			collectionURL = config.provideWorkingEnvironment(TestDataManager.TWOWORDASONESEARCH_DISNEYSEARCHPAGE);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		tree.setBaseUrl(collectionURL);
		navigateTo(collectionURL);
		setPage("home");

		Assert.assertEquals(TestDataManager.TOP50SEARCHTERMS_DISNEYSEARCHTITLE, getDriver().getTitle());
	}

	@When("^the user performs search on the word \"([^\"]*)\"$")
	public void searchFor(String searchKey) {

		inputText("searchbox", searchKey);
		clickButton("search");
		waitFor(300);
	}

	@Then("^the word should not be duplicate in 'results found' and 'did you mean'$")
	public void resultsShouldNotBeDuplicated() {

		tree.isPresent("text", "resultsfor",100);
		tree.isPresent("text", "didyoumean",100);

		Assert.assertNotSame("The 'Results Found' and 'Did you mean?' values are same!,which is not expected!",
				getText("text", "resultsfor"), getText("text", "didyoumean"));
	}
}
