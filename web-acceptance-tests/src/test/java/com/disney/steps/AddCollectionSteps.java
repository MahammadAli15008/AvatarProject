package com.disney.steps;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import com.disney.common.SmartNav;
import com.disney.utilities.managers.TestDataManager;
import com.google.common.base.Verify;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddCollectionSteps extends SmartNav {

	private List<String> attrValues = new ArrayList<String>();

	@When("^the user navigates to the New Collection Admin page$")
	public void navigateToNewCollectionAdminPage() {
		clickElement("button", "collectiondd");
		waitFor(200);
		clickElement("click", "addcollection");
		waitFor(200);

		Assert.assertTrue("New Collection Admin title does not match!",
				getText("text", "newcollectionheader").equalsIgnoreCase(TestDataManager.ADDCOLLECTIONPAGE_TITLE));
	}

	@And("^the user creates a new collection$")
	public void userCreatesANewCollection() {
		log.info("Verifying the fields in the new collection page");
		List<WebElement> list = findElements("labels", "collectionform");

		for (WebElement e : list)
			attrValues.add(e.getText());

		for (String attr : TestDataManager.ADDCOLLECTIONPAGE_FORMLABELS.split(","))
			Verify.verify(attrValues.contains(attr));

		list.clear();
		attrValues.clear();

		log.info("Entering the details for the new collection in the form!");
		isPresent("input", "collectionid");
		inputText("collectionid", TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONID);
		isPresent("input", "collectionname");
		inputText("collectionname", TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME);
		isPresent("click", "localedropdown");
		clickElement("click", "localedropdown");
		inputText("localeinput", TestDataManager.ADDCOLLECTIONPAGE_LOCALEINPUT);
		clickElement("click", "locale");
		isPresent("click", "showsharding");
		clickElement("click", "showsharding");

		log.info("Verifying the options after clicking the 'Show sharding and replication options (advanced)'  Link ");
		List<WebElement> shardingList = findElements("labels", "sharding");
		for (WebElement e : shardingList)
			attrValues.add(e.getText());

		for (String attr : TestDataManager.ADDCOLLECTIONPAGE_SHARDINGLABELS.split(","))
			Verify.verify(attrValues.contains(attr));

		shardingList.clear();
		attrValues.clear();

		log.info("Selecting the options from the Sharding & Replication Options");
		clickElement("click", "schemanamedropdown");
		clickElement("click", "replicationfactordropdown");
		clickElement("click", "internalcollectiondropdown");
		clickElement("click", "schemanamedropdown");
		clickElement("click", "dcom");

		log.info("Saving the details for creating a new collection!");
		isPresent("click", "savechanges");
		clickElement("click", "savechanges");
		refresh();
	}

	@Then("^a new collection should be created$")
	public void verifyTheNewCollectionCreated() {
		log.info("User searches for the newly created collection!");
		refresh();
		clickElement("button", "collectiondd");
		waitFor(200);
		clickElement("click", "collectionsearch");
		waitFor(200);
		inputTextWithEnter("collectionsearch", TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME);
		waitFor(300);

		Assert.assertTrue(
				"Collection is not created successfully! Collection Name mismatch!"
						+ getText("header", "createdcollectionname") + "  and  "
						+ TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME,
				getText("header", "createdcollectionname")
						.equalsIgnoreCase(TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME));

		Assert.assertTrue("Collection is not created successfully! Collection ID mismatch!"
				+ getText("text", "createdcollectionid") + "and " + TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONID,
				getText("text", "createdcollectionid")
						.equalsIgnoreCase(TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONID));

		log.info(TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME + " Collection is successfully created!");
	}
}
