package com.disney.steps;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.disney.common.SmartNav;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;

import cucumber.api.java.en.And;

public class LegalFooterAppearsSteps extends SmartNav{

	@And("the user scrolls to the bottom of the page")
	public void the_user_scrolls_to_the_bottom_of_the_page()
	{
		if(isPresent("warning", "cookie"))
		{
			if(!getAttributeValue("warning", "cookie", "style").equals("display: none;"))
			{
				waitFor(1000);
				clickButton("cookieok");
			}
		}
		scrollDown();
		log.info("Scroll down functionallity has done");
	}
	
	@And("the user can verify the \"([^\"]*)\" \"([^\"]*)\" and will be navigated to the related page")
	public void the_user_can_verify_the_footer_links(String name, String type) throws FailingHttpStatusCodeException, MalformedURLException, IOException
	{
		/*if(isPresent("warning", "cookie"))
		{
			if(!getAttributeValue("warning", "cookie", "style").equals("display: none;"))
			{
				waitFor(1000);
				clickButton("cookieok");
			}
		}*/
		if(isPresent(type, name+"1"))
		{
			List<WebElement> elements = new ArrayList<>();
			elements = findElements(type, name+1);
//			String urls = getAttributeValue(type, name+1, "href");//.split("\n");
//			System.out.println("++++++++++++++++++++++++++++" + urls);
			/*for (int i =0 ;i <urls.length(); i++)
			{
				System.out.println(urls[i]);
				URL url = new URL(urls[i]);
				System.out.println(urls[i]);
				HttpURLConnection connection = (HttpURLConnection)url.openConnection();
				connection.setRequestMethod("GET");
				connection.connect();

				int code = connection.getResponseCode();
				System.out.println("=====================" + code);
			}*/
		//}
			
			for(int i=0; i<elements.size(); i++)
			{
				System.out.println(elements.get(i).getAttribute("href"));
				log.info("Clicking on the footer link " + elements.get(i).getText());
				findElement(By.linkText(elements.get(i).getText()));
				waitFor(1000);
				/*if(isPresent("warning", "cookie"))
				{
					if(!getAttributeValue("warning", "cookie", "style").equals("display: none;"))
					{
						waitFor(1000);
						clickButton("cookieok");
					}
				}*/
				String parentWindow=getWindowHandle();
				String parentWindowUrl = getCurrentURL();
				String childWindowUrl = "";
				for (String childWindowHandle : getWindowHandles()) 
				{  
			            if(!childWindowHandle.equals(parentWindow))
			            {
			                switchToWindow(childWindowHandle);   
			                childWindowUrl = getCurrentURL();			                
			            }
			            else
			            	childWindowUrl = getCurrentURL();
			     }
				if(parentWindowUrl.equals(childWindowUrl))
				{				
					navigateBack();
					/*if(isPresent("warning", "cookie"))
					{
						if(!getAttributeValue("warning", "cookie", "style").equals("display: none;"))
						{
							waitFor(1000);
							clickButton("cookieok");
						}
					}*/
					scrollDown();					
				}
				else
				{
	                closeNewtab();
	                switchToWindow(parentWindow);
				}
				elements = findElements(type, name+1);	
			}
		}
		
		if(isPresent(type, name+"2"))
		{
			List<WebElement> elements = new ArrayList<>();
			elements = findElements(type, name+"2");
			for(int i=0; i<elements.size(); i++)
			{				
				log.info("Clicking on the footer link " + elements.get(i).getText());
				findElement(By.linkText(elements.get(i).getText()));
				waitFor(1000);
				
				/*if(isPresent("warning", "cookie"))
				{
					if(!getAttributeValue("warning", "cookie", "style").equals("display: none;"))
					{
						waitFor(1000);
						clickButton("cookieok");
					}
				}*/
				String parentWindow=getWindowHandle();
				String parentWindowUrl = getCurrentURL();
				String childWindowUrl = "";
				for (String childWindowHandle : getWindowHandles()) {  
			            if(!childWindowHandle.equals(parentWindow)){
			                switchToWindow(childWindowHandle);   
			                childWindowUrl = getCurrentURL();			                
			            }
			            else
			            	childWindowUrl = getCurrentURL();
			     }
				if(parentWindowUrl.equals(childWindowUrl))
				{				
					navigateBack();
					waitFor(1000);
					/*if(isPresent("warning", "cookie"))
					{
						if(!getAttributeValue("warning", "cookie", "style").equals("display: none;"))
						{
							waitFor(1000);
							clickButton("cookieok");
						}
					}*/
					scrollDown();					
				}
				else
				{
	                closeNewtab();
	                switchToWindow(parentWindow);
				}
				elements = findElements(type, name+"2");
			}
		}
		log.info("Clicked on the footer links ");		
	}	
}
