package com.disney.steps;

import java.util.Iterator;
import java.util.Set;
import org.junit.Assert;
import com.disney.common.SmartNav;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static com.disney.steps.RunnerTest.tree;

public class RadioDisneySteps extends SmartNav {
	String parentWindow;

	@When("^user clicks the \"([^\"]*)\"$")
	public void userClicksOnThe(String name) {
		parentWindow = getWindowHandle();
		if (isPresent("input", name)) {
			clickElementByFocus("input", name);
			for (String childWindowHandle : getWindowHandles()) {
				if (!childWindowHandle.equals(parentWindow)) {
					switchToWindow(childWindowHandle);
					waitFor(3000);// wait for child window to load the page
					log.info("switched to pop up window..");
					Assert.assertTrue("User is not able to see the pop up window.", isPresent("on", name));
					break;
				}
			}
		}
	}

	@When("^user clicks \"([^\"]*)\"$")
	public void userClicksOn(String name) {
		if (isPresent("input", name)) {
			if (tree.isPresent("cookie", "close", 2000))
				clickElement("cookie", "close");
			tree.focusElement("input", sanitize(name));
			clickElement("input", name);
			Assert.assertTrue("User is not able to click the " + name + ".", isPresent("input", name));
			log.info(name + " clicked successfully");
		}
	}

	@Then("^\"([^\"]*)\" is present$")
	public void isPresent(String name) {
		if (isPresent("input", name)) {
			Assert.assertTrue(name + " is not present.", isPresent("input", name));
			for (String childWindowHandle : getWindowHandles()) {
				if (childWindowHandle.equals(parentWindow)) {
					switchToWindow(parentWindow);
				}
			}
			log.info(name + " is present.");
		}
	}

	@Then("^\"([^\"]*)\" is played$")
	public void isPlayed(String name) {
		if (isPresent("input", name)) {
			Assert.assertTrue(name + " is not played.", isPresent("input", name));
			log.info(name + " is played.");
		}
	}

	@Then("user verify the \"([^\"]*)\" window is closed$")
	public void verifyTheWindowClosed(String element) {
		if (element.contains("popup")) {
			for (String childWindowHandle : getWindowHandles()) {
				if (childWindowHandle.equals(parentWindow)) {
					switchToWindow(parentWindow);
					log.info("User is not able to see the pop up window.");
					Assert.assertTrue("The popup window is closed", isPresent("music", "logo"));
					log.info("The popup window is closed.");
					break;
				}
			}
		} else {
			waitFor(1000);// wait for window to be closed
			boolean isClosed = getAttributeValue("window", "popoutplayer", "style").contains("display: none;");
			Assert.assertTrue("The popout player window is not closed", isClosed);
			log.info("The popout player window is closed.");
		}
	}

	@Then("^\"([^\"]*)\" are displayed successfully$")
	public void areDisplayed(String name) {
		Set<String> windows = getDriver().getWindowHandles();
		Iterator<String> windowList = windows.iterator();
		String childWindow = "";
		while (windowList.hasNext())
			childWindow = (String) windowList.next();
		switchToWindow(childWindow);
		boolean verifyAdDisplay = isPresent("input", name);
		Assert.assertTrue("User is able to verify the ads", verifyAdDisplay);
		log.info("The Radio disney popup ad is displayed.");
		switchToDefault();
	}

	@When("user is \"([^\"]*)\" radiodisney \"([^\"]*)\" window$")
	public void verifyPopOutWindowDisplayed(String type, String name) {
		parentWindow = getWindowHandle();
		for (String childWindowHandle : getWindowHandles()) {
			if (!childWindowHandle.equals(parentWindow)) {
				switchToWindow(childWindowHandle);
				Assert.assertTrue("User is not able to see the pop up window.", isPresent(type, name));
				break;
			}
		}
	}

	@When("user close the \"([^\\\"]*)\"$")
	public void closeThePopUpWindow(String element) {
		if (element.contains("popout"))
			clickElement("click", "close");
		else
			closeNewtab();
		log.info("User is able to close the " + element);
	}
}