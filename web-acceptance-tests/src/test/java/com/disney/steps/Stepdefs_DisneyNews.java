package com.disney.steps;

import static com.disney.steps.RunnerTest.tree;
import com.disney.common.SmartNav;
import com.disney.common.WorldManager;
import org.junit.Assert;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebElement;

public class Stepdefs_DisneyNews<cardGrids> extends SmartNav {
	public static String parentWindow;
	public static String parentWindowUrl;
	public static String dataTitle = "";
	public static List<WebElement> cardGrids = null;

	@Then("^user should redirect to the Disney \"([^\"]*)\" page$")
	public void userRedirectsToTheDisneyNewsPage(String element) {
		waitFor(2000);// wait for page redirect
		parentWindow = getWindowHandle();
		parentWindowUrl = getCurrentURL();
		String childWindowUrl = "";
		for (String childWindowHandle : getWindowHandles()) {
			if (!childWindowHandle.equals(parentWindow)) {
				switchToWindow(childWindowHandle);
				childWindowUrl = getCurrentURL();
			} else
				childWindowUrl = getCurrentURL();
		}
		log.info("user redirects to the url: " + getCurrentURL());
		String title = tree.getDriver().getTitle();
		if (title.contains("Magic Moments"))
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains(element) && isPresent("logo", "disney"));
		else if (title.contains("Official")) {
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains(element) && isPresent("logo", "disney"));
		} else
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains(element) && isPresent("logo", "disney"));
	}

	@When("^user click on the \"([^\"]*)\" \"([^\"]*)\" card grid$")
	public void userShouldClickOnTheCardGrid(int index, String type) {
		if (type.equalsIgnoreCase("articlepage")) {
			tree.focusElement("articlepage", "cardgrid");
			cardGrids = tree.findElements("articlepage", "cardgrid");
		} else if (type.equalsIgnoreCase("articleheadlines")) {
			tree.focusElement("article", "headlines");
			cardGrids = tree.findElements("article", "headlines");
		} else if (type.equalsIgnoreCase("More From this Author")) {
			tree.focusElement("morefromthisauthor", "article");
			cardGrids = tree.findElements("morefromthisauthor", "article");
		} else if (type.equalsIgnoreCase("Related Articles")) {
			tree.focusElement("relatedarticles", "article");
			cardGrids = tree.findElements("relatedarticles", "article");
		}
		for (int i = 0; i < cardGrids.size(); i++) {
			if (i == index - 1) {
				dataTitle = cardGrids.get(i).getAttribute("data-title");
				cardGrids.get(i).click();
				log.info("clicked on the " + dataTitle + " " + type);
				break;
			}
		}
	}

	@Then("^user should navigates to the selected \"([^\"]*)\" page$")
	public void userShouldBeNavigatedToTheSelectedPage(String page) {
		String title = "";
		waitFor(2000);// wait for page redirect
		log.info("user redirects to the url: " + getCurrentURL());
		if (page.contains("Related Articles"))
			title = dataTitle;
		else
			title = tree.getDriver().getTitle();
		log.info("user redirects to the page: " + title);
		if (!title.contains(dataTitle)) {
			Assert.assertTrue("user is not redirected to a valid page!",
					title.contains(tree.getElement("header", "page").getText()) && isPresent("logo", "disney"));
		} else
			Assert.assertTrue("user is not redirected to a valid page!",
					title.contains(dataTitle) && isPresent("logo", "disney"));
	}

	@Then("user should be able to view the contents of \"([^\"]*)\" section$")
	public void userShouldViewTheSectionContents(String section) {
		if (section.equalsIgnoreCase("Inc rich article")) {
			Assert.assertTrue("The Inc rich article image is not displayed", isPresent("incricharticle", "article"));
		} else if (section.equalsIgnoreCase("Video Player")) {
			switchToFrame(0);
			Assert.assertTrue("The Video Player is not displayed", isPresent("videoplayer", "player"));
			switchToDefault();
		} else if (section.equalsIgnoreCase("Media Image")) {
			Assert.assertTrue("The Media Image is not displayed", isPresent("mediaimage", "image"));
		} else if (section.equalsIgnoreCase("Rich text container")) {
			Assert.assertTrue("The Rich text contianer is not displayed", isPresent("richtextcontainer", "text"));
		}
	}

	@When("^user get the list of \"([^\"]*)\" articles from the grid$")
	public void user_get_the_list_of_page_content_articles(String type) {
		if (type.equalsIgnoreCase("page content")) {
			tree.focusElement("pagecontent", "cardgrid");
			cardGrids = tree.findElements("pagecontent", "cardgrid");
			log.info("Displayed of Page Content articles for the current page..: " + cardGrids.size());
		}
	}

	@Then("^user should navigates to the each \"([^\"]*)\" article pages$")
	public void user_should_redirects_to_all_the_options(String type) {
		List<WebElement> optionsList = null;
		String href = "";
		int size;
		ArrayList<String> result = new ArrayList<>();
		if (type.equalsIgnoreCase("page content")) {
			tree.focusElement("pagecontent", "cardgrid");
			optionsList = tree.findElements("pagecontent", "cardgrid");
		}
		if (cardGrids.size() >= 10)
			size = 10;
		else
			size = optionsList.size();
		for (int i = 0; i < size; i++) {
			href = optionsList.get(i).getAttribute("href");
			String optionText = optionsList.get(i).getAttribute("data-title");
			optionsList.get(i).click();
			// wait for page redirect
			waitFor(4000);
			String parentWindow = getWindowHandle();
			String parentWindowUrl = getCurrentURL();
			String url = "";
			for (String childWindowHandle : getWindowHandles()) {
				if (!parentWindow.equals(childWindowHandle)) {
					switchToWindow(childWindowHandle);
					url = getCurrentURL();
					// wait for page redirect
					waitFor(2000);
					if (url.contains(href)) {
						log.info("User redirected to the " + type + " '" + optionText + "' : " + href + " page");
						result.add("article type: " + type + "\n" + (i + 1) + ":\n option: " + optionText + "\n URL: " + href);
					}
					tree.closeActiveTab();
					switchToWindow(parentWindow);
				} else {
					url = getCurrentURL();
					if (url.contains(href))
						log.info("User redirected to the " + type + " '" + optionText + "' : " + href + " page");
					result.add("article type: " + type + "\n" + (i + 1) + ":\n option: " + optionText + "\n URL: " + href);
					// wait for page redirect
					waitFor(2000);
					tree.navigateToUrl(tree.getBaseUrl());
				}
			}
			tree.focusElement("pagecontent", "cardgrid");
			optionsList = tree.findElements("pagecontent", "cardgrid");
		}
		result.forEach(t -> WorldManager.scenario.write(t));
	}
}
