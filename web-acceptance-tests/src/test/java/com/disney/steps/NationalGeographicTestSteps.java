package com.disney.steps;

import org.junit.Assert;
import com.disney.common.SmartNav;
import com.disney.common.WorldManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import static com.disney.common.WorldManager.scenario;
import static com.disney.steps.RunnerTest.tree;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.Alert;
import org.openqa.selenium.remote.server.handler.SwitchToParentFrame;

@SuppressWarnings("unused")
public class NationalGeographicTestSteps extends SmartNav {
	public static String href = "";

	@Then("^\"([^\"]*)\" icon is displayed$")
	public void isDisplayed(String element) {
		waitFor(2000);// wait for page load
		Assert.assertTrue("The " + element + " is not present", isPresent("logo", element));
		log.info(element + " is being displayed successfully.");
	}

	@And("^user is click on the \"([^\"]*)\" \"([^\"]*)\"$")
	public void userClickedOnTheIcon(String element, String type) {
		waitFor(2000);// wait for scroll to bottom
		if (element.contains("+"))
			clickElement(type, "disneyplus");
		else {
			tree.focusElement(sanitize(type), sanitize(element));
			clickElement(type, element);
		}
		waitFor(1000);// wait for page redirect
		log.info("clicked on the " + element + " " + type);
	}

	@And("^user click on the \"([^\"]*)\" \"([^\"]*)\" button$")
	public void userClickedOnTheSlider(String type, String element) {
		if (type.contains("Video"))
			tree.scrollToElement(sanitize(type), "slidestitle");
		clickElement(type, element);
		if (tree.isAttributeValueContained(sanitize(type), "active", "aria-live", "polite"))
			log.info("automatic slider turned off..");
		if (element.contains("left"))
			log.info("clicked on the left arrow..");
		else
			log.info("clicked on the right arrow..");
	}

	@Then("^user should navigates to the \"([^\"]*)\" \"([^\"]*)\" slide$")
	public void userNavigatesToTheSlide(String type, String option) {
		int iteration = 1;
		int slidesCount = 0;
		List<WebElement> slides = tree.findElements(sanitize(type), "slides");
		for (int i = 0; i < slides.size(); i++) {
			waitFor(1000);//wait for element
			if (slides.get(i).getAttribute("style").contains("visible")) {
				log.info(type + " slide is: " + slides.get(i).getAttribute("data-title"));
				slidesCount++;
				}
			if (option.contains("Next")) {
				if (!tree.isAttributeValueContained(sanitize(type), "leftarrow", "class", "disabled", 3000))
					clickElement(type, "leftarrow");
				log.info("For next slide clicked on the left arrow..");
			} else {
				clickElement(type, "rightarrow");
				log.info("For previous slide clicked on the right arrow..");
			}
			if (slidesCount == 0) {
				if (iteration <= 3)
					i = -1;
			}
		}
		Assert.assertTrue("The user unable to view the " + type + " slide", (slidesCount > 0));
	}

	@And("^user isn mouse over on the \"([^\"]*)\" \"([^\"]*)\"$")
	public void userMouseOverOnTheIcon(String element, String type) {
		if (element.contains("+"))
			mouseOverElement(type, "disneyplus");
		else
			mouseOverElement(type, element);
		waitFor(1000);// wait for page redirect
		log.info("clicked on the " + element + " " + type);
	}
	
	@Then("^user should redirects to all the \"([^\"]*)\" \"([^\"]*)\" options$")
	public void user_should_redirects_to_all_the_options(String element, String type) {
		List<WebElement> optionsList;
		ArrayList<String> result = new ArrayList<>();
		optionsList = tree.findElements("list", sanitize(element));
		log.info("optionlist size = " + optionsList.size() + " element = " + element + "type =  " + type);
		for (int i = 0; i < optionsList.size(); i++) {
			log.info("Options " + optionsList.get(i).getText());
			href = optionsList.get(i).getAttribute("href");
			String optionText = optionsList.get(i).getAttribute("data-title");
			optionsList.get(i).click();
			waitFor(4000);// wait for page redirect
			String parentWindow = getWindowHandle();
			String parentWindowUrl = getCurrentURL();
			String url = "";
			for (String childWindowHandle : getWindowHandles()) {
				if (!parentWindow.equals(childWindowHandle)) {
					switchToWindow(childWindowHandle);
					url = getCurrentURL();
					waitFor(2000);// wait for page redirect
					if (url.contains(href)) {
						log.info("User redirected to the " + element + " '" + optionText + "' : " + href + " page");
						result.add("tab: " + element + "\n" + (i + 1) + ":\n option: " + optionText + "\n URL: " + href);
					}
					tree.closeActiveTab();
					switchToWindow(parentWindow);
				} else {
					url = getCurrentURL();
					if (url.contains(href))
						log.info("User redirected to the " + element + " '" + optionText + "' : " + href + " page");
					result.add("tab: " + element + "\n" + (i + 1) + ":\n option: " + optionText + "\n URL: " + href);
				}
			}
			tree.navigateToUrl(tree.getBaseUrl());
			waitFor(4000);// wait for page redirect
			if (element.contains("+"))
				mouseOverElement("tab", "disneyplus");
			else
				mouseOverElement("tab", element);
			waitFor(2000);// Wait for dropdown
			optionsList = tree.findElements("list", sanitize(element));
			log.info("Mouse over on the " + element + " tab");
		}
		result.forEach(t -> WorldManager.scenario.write(t));
	}

	@Then("^user is should redirects to the \"([^\"]*)\" page$")
	public void userRedirectsToTheHomePage(String element) {
		waitFor(2000);// wait for page redirect
		if (tree.isAttributeValueContained("popup", "natgeo", "style", "display: block;", 4000)) {
			clickElement("popup", "natgeoexit");
			waitFor(2000);// wait for page redirect
		}
		String parentWindow = getWindowHandle();
		String parentWindowUrl = getCurrentURL();
		String childWindowUrl = "";
		for (String childWindowHandle : getWindowHandles()) {
			if (!childWindowHandle.equals(parentWindow)) {
				switchToWindow(childWindowHandle);
				childWindowUrl = getCurrentURL();
			} else
				childWindowUrl = getCurrentURL();
		}
		log.info("user redirects to the url: " + getCurrentURL());
		String title = tree.getDriver().getTitle();
		if (title.contains("Full Movie"))
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains(element) && isPresent("icon", "nationalgeographic"));
		else if (title.contains("Disney+")) {
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains(element) || isPresent("logo", "disneyplus"));
		} else
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains(element) && isPresent("logo", "nationalgeographic"));
	}

	@Then("^user is should navigates to the \"([^\"]*)\" page$")
	public void userNavigatesToThePage(String option) {
		waitFor(2000);// wait for page redirect
		if (tree.isAttributeValueContained("popup", "natgeo", "style", "display: block;", 4000))
			clickElement("popup", "natgeoexit");
		String parentWindow = getWindowHandle();
		String parentWindowUrl = getCurrentURL();
		String childWindowUrl = "";
		for (String childWindowHandle : getWindowHandles()) {
			if (!childWindowHandle.equals(parentWindow)) {
				switchToWindow(childWindowHandle);
				childWindowUrl = getCurrentURL();
			} else
				childWindowUrl = getCurrentURL();
		}
		log.info("user redirects to the url: " + getCurrentURL());
		if (option.contains("Instagram"))
			Assert.assertTrue("user is not redirected to a Instagram valid URL", getCurrentURL().contains("natgeodocs") ||  tree.getText("element", "instagramPage").contains("Instagram") || tree.getText("element", "instagramNatgeoPage").contains("Instagram") );
		else if (option.contains("Facebook"))
			Assert.assertTrue("user is not redirected to a Facebook valid URL", getCurrentURL().contains("natgeodocs") );
		else
			Assert.assertTrue("user is not redirected to a valid URL", isPresent("element", option));
	}

	@Then("^all the images should have alt or aria-label tag$")
	public void allTheImagesShouldHaveAltTag() {
		List<WebElement> image = tree.findElements("tag", "image");
		int counterAriaLabel = 0;
		log.info("total images: " + image.size());
		for (WebElement el : image) {
			// Filter out the ctologger images
			tree.waitFor(100);
			// If alt is empty or null, add one to counter
			if (el.getAttribute("alt") == null || el.getAttribute("alt").equals("")) {
				if (el.getAttribute("aria-label") == null || el.getAttribute("aria-label").equals("")) {
					log.info("The following image does not have an aria-label and alt tag: " + el.getAttribute("src"));
					counterAriaLabel++;
				}
			}
		}
		if (counterAriaLabel == 0) {
			Assert.assertTrue("All the images are having either alt or aria-label attribute.", true);
		}
	}

	@Then("^user should view the images under \"([^\"]*)\" module$")
	public void userShouldViewTheImagesUnderVideoSlider(String type) {
		int slidesCount = 0;
		tree.scrollToElement(sanitize(type), "slidestitle");
		List<WebElement> slides = tree.findElements(sanitize(type), "slidescheck");
		List<WebElement> slidesTitle = tree.findElements(sanitize(type), "slidestitle");
		for (int i = 0; i < slides.size(); i++) {
			waitFor(500);//wait for element
			if (isAttributeContained(slides.get(i), "class", "active", 20000)) {
				log.info(type + " slide is: " + slidesTitle.get(i).getText());
				if (!tree.isAttributeValueContained(sanitize(type), "leftarrow", "class", "disabled", 3000))
					clickElement(type, "leftarrow");
				slidesCount++;
			}
			Assert.assertTrue("The user unable to view the " + type + " images", (slidesCount > 0));
		}
	}

	@And("^user click on the \"([^\"]*)\" \"([^\"]*)\" on the Video Slider$")
	public void userShouldClickOnTheVideoSlider(int index, String type) {
		int iteration = 1;
		int playcount = 0;
		List<WebElement> slides = tree.findElements("videoslider", "slides");
		if (tree.isAttributeValueContained("videoslider", "active", "aria-live", "off", 4000))
			clickElement("videoslider", "leftarrow");
		tree.scrollToElement("videoslider", "slidestitle");
		for (int i = 0; i < slides.size(); i++) {
			if (i == (index - 1)) {
				boolean isFound = isAttributeContained(slides.get(i), "aria-hidden", "false", 4000);
				if (isFound) {
					waitFor(1000);
					slides.get(i).click();
					if (isAttributeContained(slides.get(i), "class", "video-playing", 20000)) {
						log.info("clicked on the " + slides.get(i).getAttribute("data-title"));
						playcount++;
						break;
					}
				}
			} else {
				if (!tree.isAttributeValueContained("videoslider", "leftarrow", "class", "disabled", 3000))
					clickElement("videoslider", "leftarrow");
			}
			if (i == (slides.size() - 1) && playcount == 0) {
				if (iteration <= 3)
					i = -1;
			}
		}
	}

	@Then("^user can view \"([^\"]*)\" \"([^\"]*)\" video should be play$")
	public void userShouldViewtheVideoPlaying(int index, String type) {
		boolean isPlaying = false;
		tree.scrollToElement("videoslider", "slidestitle");
		List<WebElement> slides = tree.findElements("videoslider", "slides");
		isPlaying = isAttributeContained(slides.get(index - 1), "class", "video-playing", 20000);
		Assert.assertTrue("The user unable to view the video is playing ", isPlaying);
		log.info(+index + " " + type + ":" + slides.get(index - 1).getAttribute("data-title") + " is playing...");
	}

	@And("^user click on the \"([^\"]*)\" \"([^\"]*)\" on the Movie Poster$")
	public void userShouldClickOnTheMoviePoster(int index, String type) {
		tree.focusElement("movieposter", "slides");
		List<WebElement> slides = tree.findElements("movieposter", "slides");
		for (int i = 0; i < slides.size(); i++) {
			if (i == index - 1) {
				href = slides.get(i).getAttribute("data-title");
				slides.get(i).click();
				log.info("clicked on the " + href + " " + type);
				break;
			}
		}
	}

	@Then("^user should navigates to the \"([^\"]*)\" \"([^\"]*)\" page$")
	public void userNavigatesToTheMoviePage(int index, String option) {
		waitFor(2000);// wait for page redirect
		if (tree.isAttributeValueContained("popup", "natgeo", "style", "display: block;", 4000)) {
			clickElement("popup", "natgeoexit");
		}
		waitFor(2000);// wait for next window to display
		String parentWindow = getWindowHandle();
		String parentWindowUrl = getCurrentURL();
		String childWindowUrl = "";
		for (String childWindowHandle : getWindowHandles()) {
			if (!childWindowHandle.equals(parentWindow)) {
				switchToWindow(childWindowHandle);
				childWindowUrl = getCurrentURL();
			} else
				childWindowUrl = getCurrentURL();
		}
		href = href.replaceAll("\\(.*?\\) ?", "");
		log.info("user redirects to the url: " + getCurrentURL());
		Assert.assertTrue("user is not redirected to a valid URL", tree.getDriver().getTitle().contains(href));
	}
}
