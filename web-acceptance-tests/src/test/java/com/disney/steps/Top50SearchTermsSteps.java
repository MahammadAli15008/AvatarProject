package com.disney.steps;

import static com.disney.steps.RunnerTest.tree;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.net.URL;
import java.net.HttpURLConnection;

import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;

import com.disney.common.SmartNav;
import com.disney.config.WebConfig;
import com.disney.utilities.managers.TestDataManager;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Given;
import junit.framework.Assert;

public class Top50SearchTermsSteps extends SmartNav {

	private static String collectionURL = null;
	private static String disneyUSSearchURL = null;
	private WebConfig config = new WebConfig();
	private static List<String> attrValues = new ArrayList<String>();
	private static List<String> resultsFor = new ArrayList<String>();
	private static String username = System.getProperty("username");
	private static String password = System.getProperty("password");
	private List<WebElement> searchResultsPageLinks = new ArrayList<WebElement>();
	private static Map<String, String> NotFoundLinks = new HashMap<String, String>();

	@When("^the user navigates to the Disney US Search page$")
	public void navigateToDisneyUsSearchPage() {

		try {
			disneyUSSearchURL = config.provideWorkingEnvironment("disney_us_search");
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		collectionURL = TestDataManager.TOP50SEARCHTERMS_SEARCHMETRICSURL;
		setPage("home");
		tree.setBaseUrl(collectionURL);
		log.info("Navigating to the Disney US collection Search Terms page!");
		navigateTo(collectionURL);
		inputText("username", username.toUpperCase());
		waitFor(100);
		clickButton("next");
		waitFor(200);
		inputText("password", password);
		clickButton("signin");
		waitFor(200);

		Assert.assertEquals("Login unsuccessful, Welcome user is not displayed.",
				"Welcome : " + System.getProperty("username").toUpperCase(), getText("element", "welcomeuser"));
		log.info("User has successfully logged in!");
	}

	@And("^the user performs search for each search term$")
	public void userSearchesTop50SearchTermsOneByOne() {

		navigateTo(collectionURL);

		Assert.assertEquals(TestDataManager.TOP50SEARCHTERMS_DISNEYUSHEADER, getText("header", "disneyuspage"));
		log.info("Capturing the Top 50 Search Terms");
		List<WebElement> list = findElements("labels", "top50searchterms");

		for (WebElement e : list)
			attrValues.add(e.getText());

		log.info("Captured all the 50 Search Terms!");
		log.info("The list of the Top 50 Search terms : " + attrValues.toString());
		tree.setBaseUrl(disneyUSSearchURL);
		navigateTo(disneyUSSearchURL);
		log.info("Performing Search for each of the Top 50 Search Terms captured!");

		for (String searchTerm : attrValues) {
			inputText("disneysearchbar", searchTerm);
			clickButton("searchbtn");
			resultsFor.add(getText("text", "resultsfor"));
			log.info("Search Results are available for the search term : " + searchTerm);
		}
		log.info("Successfully retrieved search results for all the top 50 search terms");
	}

	@Then("^the respective search results page should be displayed$")
	public void searchResultsPageDisplays() {

		Assert.assertEquals("Results For values are different from the Search Term!", attrValues, resultsFor);
		attrValues.clear();
		resultsFor.clear();
		log.info("Success!! Search results are validated successfully for all the Search Terms");
	}

	@Given("^the user is on search results page$")
	public void userIsOnSearchResultsPage() {

		navigateToDisneyUsSearchPage();
	}

	@When("^the user performs link check for each search results page$")
	public void userChecksLinksOnEachSearchResultsPage() {

		navigateTo(collectionURL);
		Assert.assertEquals(TestDataManager.TOP50SEARCHTERMS_DISNEYUSHEADER, getText("header", "disneyuspage"));
		log.info("Capturing the Top 50 Search Terms");

		List<WebElement> list = findElements("labels", "top50searchterms");

		for (WebElement e : list)
			attrValues.add(e.getText());

		log.info("Captured all the 50 Search Terms!");
		log.info("The list of the Top 50 Search terms : " + attrValues.toString());

		for (String searchTerm : attrValues) {
			tree.setBaseUrl(disneyUSSearchURL);
			navigateTo(disneyUSSearchURL);

			inputText("disneysearchbar", searchTerm);
			clickButton("searchbtn");

			searchResultsPageLinks = getElementsByTagName("a");
			log.info("Total links present in the " + searchTerm + " results page are : "
					+ searchResultsPageLinks.size());

			log.info("Verifying the " + searchTerm + " results page for any broken links!");

			for (int i = 0; i < searchResultsPageLinks.size(); i++) {

				WebElement ele = searchResultsPageLinks.get(i);
				String url = ele.getAttribute("href");
				verifyActiveLinks(searchTerm, url);
			}
			searchResultsPageLinks.clear();
			log.info("Successfully verified all the links on the results page for the search term :" + searchTerm);
		}
	}

	public static void verifyActiveLinks(String searchTerm, String linkUrl) {

		try {
			URL url = new URL(linkUrl);
			HttpURLConnection httpURLConnect = (HttpURLConnection) url.openConnection();
			httpURLConnect.setConnectTimeout(3000);
			httpURLConnect.connect();
			if (httpURLConnect.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND)
				NotFoundLinks.put(searchTerm, linkUrl);
		} catch (Exception e) {
			e.toString();
		}
	}

	@Then("^search results page should not contain any broken links$")
	public void displayAnyBadLinks() {
		log.info("===============================================================================================");
		log.info("The broken links for each of top 50 search terms in their respective search results pages are : ");
		log.info("===============================================================================================");
		for (Map.Entry<String, String> m : NotFoundLinks.entrySet())
			log.info("Broken link for the search Term :" + m.getKey() + ", is : " + m.getValue());

		log.info("Total Number of broken links for the top 50 search terms : " + NotFoundLinks.size());
		NotFoundLinks.clear();
		attrValues.clear();
	}
}
