package com.disney.steps;

import org.junit.Assert;
import org.openqa.selenium.Alert;

import org.openqa.selenium.WebElement;

import com.disney.common.SmartNav;
import com.disney.utilities.managers.TestDataManager;
import static com.disney.steps.RunnerTest.tree;

import java.util.ArrayList;
import java.util.List;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DeleteCollectionSteps extends SmartNav {

	private HomePageSteps homePageObj = new HomePageSteps();
	private AddCollectionSteps addCollectionObj = new AddCollectionSteps();
	private String collectionName = TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME;

	@When("^user navigates to the admin page of \"([^\"]*)\" collection$")
	public void navigateToCollectionPage(String collection) {

		homePageObj.userSearchesForCollection(collection);
		homePageObj.clicksOnSearchResults();
		homePageObj.userShouldBeOnAdminPageForSelectedCollection(collection);
	}

	@When("^the user is on the Collection Admin page$")
	public void navigateToCollectionPage() {

		homePageObj.userSearchesForCollection(collectionName);

		if (tree.getText("text", "noresults").equalsIgnoreCase("NO RESULTS")) {
			refresh();
			log.info("Seems the Collection " + collectionName + " does not exist!");
			addCollectionObj.navigateToNewCollectionAdminPage();
			addCollectionObj.userCreatesANewCollection();
			addCollectionObj.verifyTheNewCollectionCreated();
		} else if (tree.getText("elements", "autosearchresults").equalsIgnoreCase(collectionName)) {
			refresh();
			addCollectionObj.verifyTheNewCollectionCreated();
			log.info("The Collection " + collectionName + " exists!");
		} else {
			List<WebElement> list = findElements("elements", "autosearchresults");
			List<String> attrValues = new ArrayList<String>();
			for (WebElement e : list)
				attrValues.add(e.getText());

			if (!attrValues.contains(collectionName)) {
				refresh();
				log.info("Seems the Collection " + collectionName + " does not exist!");
				addCollectionObj.navigateToNewCollectionAdminPage();
				addCollectionObj.userCreatesANewCollection();
				addCollectionObj.verifyTheNewCollectionCreated();
				refresh();
				attrValues.clear();
			} else {
				refresh();
				addCollectionObj.verifyTheNewCollectionCreated();
				log.info("The Collection " + collectionName + " exists!");
			}
		}
	}

	@And("^the user deletes the collection$")
	public void userDeletesCollection() {

		log.info("The user is trying to delete the collection!");
		clickElement("click", "options");
		clickElementByFocus("click", "deletecollection");
		Alert alert = getDriver().switchTo().alert();
		alert.accept();
		log.info("The collection " + collectionName + " is deleted successfully!");
	}

	@Then("^the collection should be no longer available$")
	public void verifyForDeletedCollection() {

		waitFor(300);
		refresh();
		homePageObj.userSearchesForCollection(collectionName);
		if (tree.getText("text", "noresults").equalsIgnoreCase("NO RESULTS")) {
			log.info("The Search has NO results for the collection :" + collectionName);
		} else {
			List<WebElement> list = findElements("elements", "autosearchresults");
			List<String> attrValues = new ArrayList<String>();

			for (WebElement e : list)
				attrValues.add(e.getText());

			refresh();
			Assert.assertFalse(attrValues.contains(collectionName));
			log.info("The Search has NO results for the collection :" + collectionName);
		}
	}
}
