package com.disney.steps;

import com.disney.common.CommonStepHelpers;
import com.disney.common.SmartNav;
import com.disney.utilities.managers.TestDataManager;
import com.google.common.base.Verify;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;

public class CollectionAdminPageSteps extends SmartNav {

	private HomePageSteps homePageObj = new HomePageSteps();
	private CommonStepHelpers helperObj = new CommonStepHelpers();
	private List<WebElement> list;
	private List<String> attrValues = new ArrayList<String>();

	@When("^user is on \"([^\"]*)\" tab for \"([^\"]*)\" collection$")
	public void userIsOnTabForCollection(String tab, String collection) {
		homePageObj.userSearchesForCollection(collection);
		homePageObj.clicksOnSearchResults();
		homePageObj.userShouldBeOnAdminPageForSelectedCollection(collection);
		switch (tab) {
		case "view":
			clickElement("click", "viewtab");
			break;

		case "edit":
			clickElement("click", "edittab");
			break;

		case "stats":
			clickElement("click", "statstab");
			break;

		case "search metrics":
			clickElement("click", "searchmetricstab");
			break;

		default:
			System.out.println("Invalid tab!");
			break;
		}
	}

	@Then("^content should populate in View tab$")
	public void contentInViewTab() {

		log.info("Validating the contents under View tab");

		clickElement("click", "basicdetailstab");
		log.info("Validating the table contents in the Basic Details tab under View tab");
		list = findElements("view", "basicdetailslabels");
		for (WebElement e : list)
			attrValues.add(e.getText());

		for (String attr : TestDataManager.COLLECTIONADMINPAGE_VIEW_BASICDETAILSTAB.split(",")) {
			Verify.verify(attrValues.contains(attr));
		}
		list.clear();
		attrValues.clear();

		clickElement("click", "datasourcestab");
		log.info("Validating the table contents in the Datasources tab under View tab");
		list = findElements("view", "datasourceslabels");
		for (WebElement e : list)
			attrValues.add(e.getText());

		for (String attr : TestDataManager.COLLECTIONADMINPAGE_VIEW_DATASOURCESTAB.split(",")) {
			Verify.verify(attrValues.contains(attr));
		}
		list.clear();
		attrValues.clear();

		clickElement("click", "redirectstab");
		log.info("Validating the table contents in the Redirects tab under View tab");
		String actualValue = getText("view", "redirectslabels");
		String expValue = TestDataManager.COLLECTIONADMINPAGE_VIEW_REDIRECTSTAB;

		Assert.assertTrue(
				"Expected the Redirects tab contains the field name : " + expValue + " actually found " + actualValue,
				actualValue.equalsIgnoreCase(expValue));
		log.info("Successfully Validated the contents under View tab");
	}

	@Then("^content should populate in Edit tab$")
	public void contentInEditTab() {

		log.info("Validating the contents under Edit tab");

		clickElement("click", "datasourcestab");
		log.info("Validating the table contents in the Datasources Tab under Edit tab");
		list = findElements("edit", "datasourceslabels");
		for (WebElement e : list)
			attrValues.add(e.getText());

		for (String attr : TestDataManager.COLLECTIONADMINPAGE_EDIT_DATASOURCESTAB.split(","))
			Verify.verify(attrValues.contains(attr));

		list.clear();
		attrValues.clear();

		clickElement("click", "redirectstab");
		log.info("Validating the table contents in the Redirects Tab under Edit tab");
		String actualValue = getText("edit", "redirectheader");
		String expValue = TestDataManager.COLLECTIONADMINPAGE_EDIT_REDIRECTSTAB;

		Assert.assertTrue(
				"Expected the Redirects tab contains the field name : " + expValue + " actually found " + actualValue,
				actualValue.equalsIgnoreCase(expValue));
		log.info("Successfully Validated the contents under Edit tab");
	}

	@Then("^content should populate in Stats tab$")
	public void contentInStatsTab() {

		log.info("Validating the contents under Stats tab");

		list = findElements("stats", "indexlabels");
		for (WebElement e : list)
			attrValues.add(e.getText());

		for (String attr : TestDataManager.COLLETIONADMINPAGE_STATS_INDEX.split(","))
			Verify.verify(attrValues.contains(attr));

		list.clear();
		attrValues.clear();
		log.info("Successfully Validated the contents under Stats tab");
	}

	@Then("^content should populate in Search Metrics tab$")
	public void contentInSearchMetricsTab() {

		log.info("Validating the contents under Search Metrics tab");

		clickElement("click", "searchtab");
		log.info("Validating the table contents in the Search Tab");
		helperObj.validateSearchTableContent("searchsearchtermlabels", "searchnoofsearcheslabels");

		clickElement("click", "autocompletetab");
		log.info("Validating the table contents in the Autocomplete Tab");
		helperObj.validateSearchTableContent("autocompletesearchtermlabels", "autocompletesearchnoofsearcheslabels");

		clickElement("click", "instantsearchtab");
		log.info("Validating the table contents in the Instant Search Tab");
		helperObj.validateSearchTableContent("instantsearchtermlabels", "instantsearchnoofsearcheslabels");

		clickElement("click", "recommendationstab");
		log.info("Validating the table contents in the Recommendations Tab");
		helperObj.validateSearchTableContent("recommendationssearchtermlabels", "instantsearchnoofsearcheslabels");

		clickElement("click", "advancesearchestab");
		log.info("Validating the table contents in the Advance Searches Tab");
		helperObj.validateSearchTableContent("advancesearchessearchtermlabels", "advancesearchesnoofsearcheslabels");

		log.info("Successfully Validated the contents under Search Metrics tab");
	}
}
