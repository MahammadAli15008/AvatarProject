package com.disney.steps;

import static com.disney.steps.RunnerTest.tree;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;
import org.json.JSONObject;
import org.junit.Assert;

import com.disney.common.SmartNav;
import com.disney.config.WebConfig;
import com.disney.utilities.managers.TestDataManager;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class BabbleEndpointSteps extends SmartNav {

	private static String collectionURL = null;
	private WebConfig config = new WebConfig();

	@Given("^the MH Search endpoint$")
	public void searchEndpoint() {
		try {
			collectionURL = config.provideWorkingEnvironment("adminhome");
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	@Given("^the user perform \"([^\"]*)\" with keyword \"([^\"]*)\"$")
	public void searchUrl(String key, String endpoint) {
		String searchUrl;
		if (key.equalsIgnoreCase("search")) {
			searchUrl = collectionURL + TestDataManager.BABBLEENDPOINT_SEARCH + endpoint;
			tree.setBaseUrl(searchUrl);
			navigateTo(searchUrl);
		} else {
			searchUrl = collectionURL + TestDataManager.BABBLEENDPOINT_AUTOCOMPLETESEARCH + endpoint;
			tree.setBaseUrl(searchUrl);
			navigateTo(searchUrl);
		}
		setPage("home");
	}

	@Then("^the results returned should contain the search key \"([^\"]*)\"$")
	public void searchResults(String searchWord) {
		log.info("Verifying the response contains the results for " + searchWord);
		String json = getText("text", "responsebody");
		JSONObject obj = new JSONObject(json);
		Assert.assertTrue(obj.getInt("total_found") != 0);
		json.contains(searchWord);
	}
}
