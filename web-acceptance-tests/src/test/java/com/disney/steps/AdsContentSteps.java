package com.disney.steps;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import com.disney.common.SmartNav;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AdsContentSteps  extends SmartNav{
	
/*	@When("^user confirms the banner ad of \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_confirms_the_banner_ad(String name, String type)
	{
		Assert.assertTrue(isPresent(type, name));
	}*/
/*	@When("^user confirms the \"([^\"]*)\" of \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_confirms_the_banner_ad(String name,String collection,String type)
	{
		log.info("Loading the ads of "+collection+" collection");
		Assert.assertTrue(isPresent(type, name));
	}*/
	@When("^user confirms the number of \"([^\"]*)\" of \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_confirms_number_of_ads(String name,String collection,String type)
	{
		int numberOfDisplayedAds=0;
	
		List <WebElement> numberOfAds=findElements(type, name);	
		waitFor(1000);
		for(WebElement elements:numberOfAds){
			if((elements.getAttribute("style").toString().contains("border: 0pt none;"))){
				numberOfDisplayedAds++;
			}
		}
		log.info("Total number of Ads Present in the " + collection + " collection are " + numberOfAds.size());
		log.info("Total number of displayed ads for " + collection + " collection are " + numberOfDisplayedAds);
		Assert.assertTrue("Excepted atlest one add is loaded but actually found" + numberOfDisplayedAds, numberOfDisplayedAds!=0);
	}
	@Then("^user confirms the \"([^\"]*)\" \"([^\"]*)\" in the \"([^\"]*)\" page$")
	public void user_confirms_the_ad(String name, String type, String page)
	{
		setPage(page);
		if(isPresent(type, name))
		{
			List<WebElement> elements = new ArrayList<>();
			
			elements = findElements(type, name);
			log.info("Number of ads found for the collection url " + getCurrentURL() + ": " + elements.size());
		}	
		else
			log.info("No ads found for the collection url " + getCurrentURL());
	}
	
	@And("^clicks on the \"([^\"]*)\" \"([^\"]*)\"$")
	public void clicks_on_the_ad(String name, String type)
	{
		clickElement(type, name);			
	}
	
	@And("^it navigates to the related page$")
	public void it_navigates_to_the_related_page()
	{
		String parentWindow=getWindowHandle();
		String parentWindowUrl = getCurrentURL();
		String childWindowUrl = "";
		for (String childWindowHandle : getWindowHandles()) 
		{  
	            if(!childWindowHandle.equals(parentWindow))
	            {
	                switchToWindow(childWindowHandle);   
	                childWindowUrl = getCurrentURL();			                
	            }
	            else
	            	childWindowUrl = getCurrentURL();
	     }
		if(parentWindowUrl.equals(childWindowUrl))
			navigateBack();
		else
		{
            closeNewtab();
            switchToWindow(parentWindow);
		}
		navigateBack();
	}
}
