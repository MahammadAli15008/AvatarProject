package com.disney.steps;

import java.util.ArrayList;
import java.util.List;
import static com.disney.steps.RunnerTest.tree;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import com.disney.common.SmartNav;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class SearchSteps extends SmartNav {

	String parentWindow = getWindowHandle();
	String parentWindowUrl;
	String childWindowUrl;

	@And("^clicks on the \"([^\"]*)\" \"([^\"]*)\" field located on the chrome with input text as \"([^\"]*)\"$")
	public void clicks_on_the_search_text_field_located_on_the_chrome(String name, String type, String inputText) {
		parentWindowUrl = getCurrentURL();
		if (parentWindowUrl.contains("search")) {
			clickElement(type, name);
			inputTextWithEnter(name, inputText);
		} else {
			clickElement(type, name);
			WebElement resultList = tree.input(type, name, inputText);
			if (isPresent("autolist", "search"))
				resultList.sendKeys(Keys.ENTER);
			else {
				clickElement(type, name);
				tree.inputWithEnter(type, name, inputText);
			}
		}
	}
	
	@And("^clicks on the \"([^\"]*)\" \"([^\"]*)\" field located on the chrome with input text \"([^\"]*)\"$")
	public void clicks_on_the_search_text_field_located_on_chrome(String name, String type, String searchText) {
		parentWindowUrl = getCurrentURL();
		/*
		 * if(isPresent("warning", "cookie")) { if(getAttributeValue("warning",
		 * "cookie", "style").equals("")) clickButton("cookieok"); }
		 */
		clickElement(type, name);
		inputText(name, searchText);
	}

	@Then("^user will be redirected to a new page with their search results \"([^\"]*)\"$")
	public void user_will_be_redirected_to_a_new_page_with_their_search_results(String searchText) {
		waitFor(2000);//wait for redirect to the page
		for (String childWindowHandle : getWindowHandles()) {
			if (!childWindowHandle.equals(parentWindow)) {
				switchToWindow(childWindowHandle);
				childWindowUrl = getCurrentURL();
			} else
				childWindowUrl = getCurrentURL();
		}
		Assert.assertTrue("Search results page not contains the" + "'" + searchText + "'",
				childWindowUrl.contains(searchText));
		log.info("Search results page contains the search term: " + "'" + searchText + "'");
	}

	@Then("^the \"([^\"]*)\" \"([^\"]*)\" will display an autocomplete suggestion$")
	public void the_autocomplete_input_will_display_an_completed_suggestion(String name, String type) {
		Assert.assertTrue(isPresent(type, name));
	}

	@And("^clicks on one of the search filters which contains the text \"([^\"]*)\"$")
	public void clicks_on_one_of_the_search_filters(String searchText) {
		List<WebElement> elements = new ArrayList<>();
		elements = findElements("search", "autocompletelist");

		elements.get(0).click();
	}

	/*
	 * @And(
	 * "^the search will show items related only to the filtere text \"([^\"]*)\"$"
	 * ) public void
	 * the_search_will_show_items_related_only_to_the_filtere_text(String
	 * searchText) { Assert.assertTrue("",
	 * getCurrentURL().contains(searchText)); }
	 */

	@And("^user clicks on the view all results link$")
	public void user_clicks_on_the_view_all_results_link() {
		/*
		 * if(isPresent("warning", "cookie")) { if(getAttributeValue("warning",
		 * "cookie", "style").equals("")) clickButton("cookieok"); }
		 */
		waitFor(1000);
		clickElement("search", "viewallresults");
	}

	@Then("^the search will display additional search results for the input text \"([^\"]*)\"$")
	public void the_search_will_display_additional_search_results(String searchText)
	{
		Assert.assertTrue(
				"Expected the currect url contain the text " + searchText + ", actually found " + getCurrentURL(),
				getCurrentURL().contains(searchText));
	}

	@And("^user enters \"([^\"]*)\" in the \"([^\"]*)\" \"([^\"]*)\" field$")
	public void user_enters_input_in_the_search_field(String character, String name, String type)
	{
		clickElement(type, name);
		inputText(name, character);
	}

	@Then("^results should be returned in the dropdown$")
	public void results_should_be_returned_in_the_dropdown() 
	{
		Assert.assertTrue(isPresent("search", "autoresults"));
	}

	@Then("^results should have an anchor link$")
	public void results_should_have_an_anchor_link() 
	{
		List<WebElement> elements = new ArrayList<>();
		elements = findElements("search", "autoresultsanchorlink");
		for (int i = 1; i < elements.size(); i++) 
		{
			String autoresult = elements.get(i).getAttribute("href");
			Assert.assertTrue("Auto Results doesn't have anchor link", autoresult.startsWith("http"));
		}
	}

	@And("^user clicks on the \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_clicks_on_the_link(String name, String type) 
	{
		if(name.equalsIgnoreCase("show more"))
		{
			WebElement element = getElement(type, name);
			Actions actions = new Actions(getDriver());
			actions.moveToElement(element).click().perform();
		}
		else
		{		
			List<WebElement> elements = new ArrayList<>();
			elements = findElements(type, "icons");
			for (int i = 1; i < elements.size(); i++) 
			{
				 if((elements.get(i).getAttribute("href")).contains(name))
				 {
					 elements.get(i).click();
					 break;
				 }			
			}		  
		}
		waitFor(2000);
	}

	@Then("^all \"([^\"]*)\" \"([^\"]*)\" contain the \"([^\"]*)\"$")
	public void all_product_links_contains_the_affiliate_query_param(String name, String type, String affiliateQueryParam)
	{
		List<WebElement> elements = new ArrayList<>();
		elements = findElements(sanitize(type), sanitize(name));
		for (int i = 1; i < elements.size(); i++) {
			String link = elements.get(i).getAttribute("href");
			Assert.assertTrue(name + " links doesn't have affiliate param", link.contains(affiliateQueryParam));
		}
		log.info(name + " links contains the affiliate param: " + "'" + affiliateQueryParam + "'");
	}

	@Then("^the page should display additional results for the input text \"([^\"]*)\"$")
	public void the_page_should_display_additional_results(String searchText)
	{
		Assert.assertTrue("Expected the current url contain the text " + searchText + ", actually found " + getCurrentURL(),
				getCurrentURL().contains(searchText));
	}
}
