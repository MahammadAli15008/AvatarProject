package com.disney.steps;

import static com.disney.steps.RunnerTest.tree;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;
import org.json.JSONObject;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.NoAlertPresentException;

import com.disney.common.SmartNav;
import com.disney.config.WebConfig;
import com.disney.utilities.managers.TestDataManager;
import com.google.common.base.Verify;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Given;
import junit.framework.Assert;

public class IndexingSteps extends SmartNav {

	private AddCollectionSteps addCollectionObj = new AddCollectionSteps();
	private HomePageSteps homePageObj = new HomePageSteps();
	private String collectionURL = null;
	private WebConfig config = new WebConfig();
	private List<String> attrValues = new ArrayList<String>();

	@And("^the collection is already created$")
	public void checkCollection() {

		homePageObj.userSearchesForCollection(TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME);
		if (tree.getText("text", "noresults").equalsIgnoreCase("NO RESULTS")) {
			refresh();
			log.info("Seems the Collection " + TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME + " does not exist!");
			addCollectionObj.navigateToNewCollectionAdminPage();
			addCollectionObj.userCreatesANewCollection();
			addCollectionObj.verifyTheNewCollectionCreated();
		} else if (tree.getText("elements", "autosearchresults")
				.equalsIgnoreCase(TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME)) {
			refresh();
			log.info("The Collection " + TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME + " exists!");
		} else {
			List<WebElement> list = findElements("elements", "autosearchresults");
			List<String> attrValues = new ArrayList<String>();
			for (WebElement e : list)
				attrValues.add(e.getText());

			if (!attrValues.contains(TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME)) {
				refresh();
				log.info("Seems the Collection " + TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME
						+ " does not exist!");
				addCollectionObj.navigateToNewCollectionAdminPage();
				addCollectionObj.userCreatesANewCollection();
				addCollectionObj.verifyTheNewCollectionCreated();
				refresh();
				attrValues.clear();
			} else {
				refresh();
				log.info("The Collection " + TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME + " exists!");
			}
		}
	}

	@When("^the user navigates to the custom sources url$")
	public void navigateToCustomSourcesURL() {

		try {
			collectionURL = config.provideWorkingEnvironment("adminhome");
		} catch (ParserConfigurationException | SAXException | IOException e) {
			log.error("Exception occured!! :" + e.toString());
		}
		String customSourcesURL = collectionURL + "admin/collections/"
				+ TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONID.toLowerCase() + "/customsources";
		tree.setBaseUrl(customSourcesURL);

		log.info("Navigating to the page for the collection type "
				+ TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONID.toLowerCase() + " with the URL " + customSourcesURL);
		navigateTo(customSourcesURL);
		setPage("home");
	}

	@Then("^the indexer URLS fields should be displayed in the page$")
	public void indexerURLFieldsShouldDisplay() {

		log.info("Verifying the Indexer URLS fields in the custom sources page");
		List<WebElement> list = findElements("labels", "indexerurlfields");

		for (WebElement e : list)
			attrValues.add(e.getText());

		for (String attr : TestDataManager.INDEXING_CUSTOMSOURCESLABELS.split(","))
			Verify.verify(attrValues.contains(attr));

		list.clear();
		attrValues.clear();
		tree.isPresent("link", "indexingoptions", 100);
		clickElement("link", "indexingoptions");
		tree.isPresent("label", "clean", 100);
		tree.isPresent("input", "clean", 100);
		clickElement("input", "clean");
		clickElement("input", "clean");
		tree.isPresent("label", "purge", 100);
		tree.isPresent("input", "purge", 100);
		clickElement("input", "purge");
		clickElement("input", "purge");
		tree.isPresent("button", "indexall", 100);
	}

	@And("^the user indexes the collection with custom data source$")
	public void populateCMSURLField() {

		log.info("Populating the CMS URL field and then selecting the Index option");
		inputText("cmsurlfield", TestDataManager.INDEXING_CMSURLFIELDDATA);
		clickButton("cmsindex");
	}

	@Then("^indexing with \"([^\"]*)\" should be successful$")
	public void indexingShouldBeSuccessful(String indexingSource) {

		tree.isPresent("image", "spinneranimation", 100);
		tree.isPresent("text", "timeelapsed", 100);
		log.info("Indexing is successful !! A Loading Animation and the TimeLapsed are displayed successfully!");
		waitFor(500);
		if (indexingSource.equalsIgnoreCase("datasources"))
			getDriver().switchTo().alert().accept();
	}

	@Given("^the user performs \"([^\"]*)\" for \"([^\"]*)\"$")
	public void searchUrl(String searchType, String searchKey) {

		String searchUrl;
		if (searchType.equalsIgnoreCase("search")) {
			try {
				searchUrl = config.provideWorkingEnvironment("adminhome")
						+ TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONID.toLowerCase()
						+ TestDataManager.INDEXING_CUSTOMSOURCESEARCH + searchKey;
				tree.setBaseUrl(searchUrl);
				navigateTo(searchUrl);
			} catch (ParserConfigurationException | SAXException | IOException e) {
				log.error("Exception occured!! :" + e.toString());
			}
		} else {
			try {
				searchUrl = config.provideWorkingEnvironment("adminhome")
						+ TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONID.toLowerCase()
						+ TestDataManager.INDEXING_CUSTOMSOURCESAUTOCOMPLETE + searchKey;
				tree.setBaseUrl(searchUrl);
				navigateTo(searchUrl);
			} catch (ParserConfigurationException | SAXException | IOException e) {
				log.error("Exception occured!! :" + e.toString());
			}
		}
		setPage("home");
	}

	@Then("^response should contain some search results for \"([^\"]*)\"$")
	public void containsSearchResults(String searchWord) {

		log.info("Verifying the response contains the results for" + searchWord);
		String json = getText("text", "responsebody");
		JSONObject obj = new JSONObject(json);
		Assert.assertTrue(obj.getInt("total_found") != 0);
		json.contains(searchWord);
	}

	@When("^the user navigates to the datasources tab$")
	public void navigateToDataSources() {

		homePageObj.userSearchesForCollection(TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONNAME);
		clickElement("text", "highlight");
		clickElement("tab", "edit");
		clickElement("tab", "datasources");
	}

	@And("^the user indexes the collection with data sources$")
	public void populateCMSURLFieldInDataSources() {

		log.info("Populating the CMS URL field and selects Index option");
		inputText("cmsurlfield", TestDataManager.INDEXING_CMSURLFIELDDATA);
		clickButton("saveindex");
	}

	@When("^the user navigates to the index sources url$")
	public void navigateToIndexSourcesURL() {

		try {
			collectionURL = config.provideWorkingEnvironment("adminhome");
		} catch (ParserConfigurationException | SAXException | IOException e) {
			log.error("Exception occured!! :" + e.toString());
		}

		String customSourcesURL = collectionURL + "admin/collections/"
				+ TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONID.toLowerCase() + "/indexsources";
		tree.setBaseUrl(customSourcesURL);

		log.info("Navigating to the page for the collection type "
				+ TestDataManager.ADDCOLLECTIONPAGE_COLLECTIONID.toLowerCase() + " with the URL " + customSourcesURL);
		navigateTo(customSourcesURL);
		setPage("home");
	}

	@And("^the user selects the index option for the cms url$")
	public void clickIndexButtonOfCMSURL() {

		log.info("Selecting/clicking the Index button of CMS URL field");
		clickButton("cmsindex");
	}
}
