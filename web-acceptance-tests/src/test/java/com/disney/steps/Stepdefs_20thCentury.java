package com.disney.steps;

import static com.disney.steps.RunnerTest.tree;
import static io.restassured.RestAssured.*;
import com.disney.common.SmartNav;
import org.junit.Assert;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;
import io.restassured.response.Response;
import javax.xml.parsers.ParserConfigurationException;
import cucumber.api.java.en.When;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import static com.disney.steps.RunnerTest.environment;
import com.disney.utilities.managers.ExternalPropertiesManager;

public class Stepdefs_20thCentury extends SmartNav {
	public static String parentWindow;
	public static String parentWindowUrl;
	public static String href = "";
	private List<WebElement> slides = null;
	private Response response;
	private StringBuilder unSuccessfulURLs = new StringBuilder();
	private StringBuilder successfulURLs = new StringBuilder();
	private int successfulUrlsCount = 0;
	private int unSuccessfulUrlsCount = 0;

	public ExternalPropertiesManager provideExternalProperties() {
		return new ExternalPropertiesManager();
	}

	@Then("^user should redirect to the \"([^\"]*)\" page$")
	public void userRedirectsToThePage(String element) {
		waitFor(2000);// wait for page redirect
		parentWindow = getWindowHandle();
		parentWindowUrl = getCurrentURL();
		String childWindowUrl = "";
		for (String childWindowHandle : getWindowHandles()) {
			if (!childWindowHandle.equals(parentWindow)) {
				switchToWindow(childWindowHandle);
				childWindowUrl = getCurrentURL();
			} else
				childWindowUrl = getCurrentURL();
		}
		log.info("user redirects to the url: " + getCurrentURL());
		String title = tree.getDriver().getTitle();
		if (title.contains("Official"))
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains(element) && isPresent("logo", "20thcenturystudios"));
		else if (title.contains("Movies")) {
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains(element) && isPresent("logo", "20thcenturystudios"));
		} else
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains(element) && isPresent("logo", "20thcenturystudios"));
	}

	@Then("^user should navigate to the \"([^\"]*)\" page$")
	public void userShouldNavigateToThePage(String option) {
		waitFor(2000);// wait for page redirect
		handleMailPopup();
		switchToWindow(parentWindow);
	}

	@And("^the user clicked on the \"([^\"]*)\" \"([^\"]*)\"$")
	public void userClickedOn(String element, String type) {
		waitFor(2000);// wait for scroll to bottom
		parentWindow = getWindowHandle();
		parentWindowUrl = getCurrentURL();
		tree.focusElement(sanitize(type), sanitize(element));
		clickElement(type, element);
		waitFor(1000);// wait for page redirect
		log.info("clicked on the " + element + " " + type);
	}
	
	@Then("^user should navigates to the 'Do Not Sell My Info' page$")
	public void user_should_navigates_to_the_page() throws Throwable {
		waitFor(2000);// wait for page redirect
		if (tree.isAttributeValueContained("popup", "natgeo", "style", "display: block;", 4000))
			clickElement("popup", "natgeoexit");
		log.info("user redirects to the url: " + getCurrentURL());
	 
		Assert.assertTrue("user is not redirected to a valid URL", isPresent("element", "donotsellmyinfoassertionelement"));
	}

	@And("^user click on the \"([^\"]*)\" \"([^\"]*)\" on the tiles$")
	public void userShouldClickOnTheTiles(int index, String type) {
		if (type.contains("Featured")) {
			tree.focusElement("featuredtitles", "slides");
			slides = tree.findElements("featuredtitles", "slides");
		} else if (type.contains("BLU-RAY")) {
			tree.focusElement("onbluray", "slides");
			slides = tree.findElements("onbluray", "slides");
		} else if (type.contains("VIDEOS")) {
			tree.focusElement("videosection", "slides");
			slides = tree.findElements("videosection", "slides");
		} else if (type.contains("DVD")) {
			tree.focusElement("dvd", "slides");
			slides = tree.findElements("dvd", "slides");
		} else if (type.contains("RECOMMENDED")) {
			tree.focusElement("recommendedmovies", "slides");
			slides = tree.findElements("recommendedmovies", "slides");
		} else if (type.contains("IMAGE GALLERY")) {
			tree.focusElement("imagegallery", "slides");
			slides = tree.findElements("imagegallery", "slides");
		}
		for (int i = 0; i < slides.size(); i++) {
			if (i == index - 1) {
				href = slides.get(i).getAttribute("data-title");
				slides.get(i).click();
				log.info("clicked on the " + href + " " + type);
				break;
			}
		}
	}

	@Then("^user should be able to view page contents of \"([^\"]*)\"$")
	public void userVerfiesThePageContentsOf(String page) {
		String title = tree.getDriver().getTitle();
		if (page.contains("The New Mutants")) {
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains("The New Mutants") && isPresent("logo", "20thcenturystudios"));
		} else if (page.contains("The Call of the Wild")) {
			Assert.assertTrue("user is not redirected to a valid URL",
					title.contains("The Call of the Wild") && isPresent("logo", "20thcenturystudios"));
		}
		Assert.assertTrue("WATCH TRAILER button is not displayed", isPresent("link", "watchtrailer"));
		Assert.assertTrue("Movie Details are not displayed!", isPresent("text", "movietitle"));
	}

	@When("^the user hits a collection of 20th Century FE Urls$")
	public void user_hits_a_collection_of_20thCentury_FE_Urls()
			throws ParserConfigurationException, SAXException, IOException {
		environment = provideExternalProperties().getExternalEnvironmentProperty();
		File currDir = new File(".");
		String dirPath = currDir.getAbsolutePath().replace(".", "");
		dirPath = dirPath + "src\\test\\resources\\com\\disney\\properties\\20thCentury_Urls_" + environment + ".txt";
		BufferedReader br = null;
		FileReader fr = null;
		fr = new FileReader(dirPath.replace("\\", File.separator));
		br = new BufferedReader(fr);
		String url;
		while ((url = br.readLine()) != null) {
			if (!url.equals("")) {
				if (url.contains("YYYYMMDD")) {
					String d = String.valueOf(LocalDate.now()).replace("-", "");
					url = url.replace("YYYYMMDD", d);
					response = given().when().header("Pragma", "no-cache").get(url);
				} else
					response = given().when().header("Pragma", "no-cache").get(url);
				if (environment.equalsIgnoreCase("prod")) {
					if ((response.getStatusCode() == 200) || (response.getStatusCode() == 301)) {
						successfulURLs.append(url + " = " + response.getStatusCode() + "\n");
						successfulUrlsCount++;
					} else {
						unSuccessfulURLs.append(url + " = " + response.getStatusCode() + "\n");
						unSuccessfulUrlsCount++;
					}
				} else {
					if (response.getStatusCode() != 200) {
						unSuccessfulURLs.append(url + " = " + response.getStatusCode() + "\n");
						unSuccessfulUrlsCount++;
					} else {
						successfulURLs.append(url + " = " + response.getStatusCode() + "\n");
						successfulUrlsCount++;
					}
				}
			}
		}
		log.info("Total no of urls executed: " + (successfulUrlsCount + unSuccessfulUrlsCount));
		log.info("No of Urls with successful status code: " + successfulUrlsCount);
		log.info("successful FE Url's " + successfulURLs);
		br.close();
	}

	@And("^list of 20th century FE Urls status should be 200$")
	public void list_of_FE_urls_status_code() {
		Assert.assertTrue("Expected the list of FE Urls status code should be 200, but actually found "
				+ unSuccessfulUrlsCount + " unsuccessful Url's \n" + unSuccessfulURLs, unSuccessfulUrlsCount == 0);
	}

	@And("^list of 20th century FE Urls status should be 301$")
	public void list_of_FE_urls_status_code_should_be_301() {
		Assert.assertTrue("Expected the list of FE Urls status code should be 301, but actually found "
				+ unSuccessfulUrlsCount + " unsuccessful Url's \n" + unSuccessfulURLs, unSuccessfulUrlsCount == 0);
	}
}
