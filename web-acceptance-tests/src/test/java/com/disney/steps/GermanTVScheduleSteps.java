package com.disney.steps;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import com.disney.common.SmartNav;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GermanTVScheduleSteps  extends SmartNav {
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	Calendar calendar = Calendar.getInstance();
	
	// Todays Date
	String todaysDate = sdf.format(calendar.getTime());
	String tomorrowsDate;
	String yesterdaysDate;
	
	@Then("^the user should be presented with the \"([^\"]*)\" \"([^\"]*)\"$")
	public void the_user_should_be_presented_with_the_calendar(String type, String name)
	{
		Assert.assertTrue("Expected the calender filter is present, but actually not found", isPresent(type, name));
	}
	
	@And("^validate if today, previous and tomorrow dates are available in the \"([^\"]*)\" \"([^\"]*)\"$")
	public void validate_if_today_tomorrow_and_yesterday_dates_are_available(String type, String name)
	{	
		// Tomorrows Date
		calendar.add(Calendar.DAY_OF_YEAR,1);
		tomorrowsDate = sdf.format(calendar.getTime());

		// Yesterdays Date
		calendar.add(Calendar.DAY_OF_YEAR,-2);
		yesterdaysDate = sdf.format(calendar.getTime());
		
		List<WebElement> elements = new ArrayList<>();
		elements = findElements(type, name);
		
		List<String> dates = new ArrayList<String>();
		dates.add(yesterdaysDate);
		dates.add(todaysDate);
		dates.add(tomorrowsDate);
		
		List<String> applicationDates = new ArrayList<String>();
		for(int i=0; i<elements.size(); i++)
		{
			if(elements.get(i).getAttribute("data-date").equals(yesterdaysDate) || elements.get(i).getAttribute("data-date").equals(todaysDate) || elements.get(i).getAttribute("data-date").equals(tomorrowsDate))
				applicationDates.add(elements.get(i).getAttribute("data-date"));
		}
		
		Assert.assertEquals("Expcted yesterday's, today's and tomorrow's dates as " + dates + ", but actually found " + applicationDates,
				dates, applicationDates);
	}

	@And("^validate todays date is selected in the \"([^\"]*)\" \"([^\"]*)\"$")
	public void validate_todays_date_is_selected_in_the_calendar(String type, String name)
	{		
		List<WebElement> elements = new ArrayList<>();
		elements = findElements(type, name);
		
		String selectedDate = null;
		for(int i=0; i<elements.size(); i++)
		{
			if(elements.get(i).getAttribute("class").contains("active"))
				selectedDate = elements.get(i).getAttribute("data-date");
		}
		
		Assert.assertEquals("Expcted selected as " + todaysDate + ", but actually found " + selectedDate,
				todaysDate, selectedDate);
	}
	
	@When("^user scroll down the page$")
	public void user_scroll_down_the_page()
	{
		scrollDown();
	}
	
	@Then("^the user can view the \"([^\"]*)\" \"([^\"]*)\" at the top$")
	public void the_user_can_view_the_calendar_at_the_top(String type, String name)
	{
		Assert.assertTrue("Expected the calender filter is present, but actually not found", isPresent(type, name));
	}
	
	@When("^user clicks on previous date from the \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_clicks_on_previous_date_from_the_calendar(String type, String name)
	{	
		refresh();
		log.info("Clicking on the Previous date " + yesterdaysDate);
		isPresent(type, name);
		List<WebElement> elements = new ArrayList<>();
		elements = findElements(type, name);
		
		String selectedDate = null;
		for(int i=0; i<elements.size(); i++)
		{
			log.info("Trying to clicking on the Element " + elements.get(i).getAttribute("data-date"));
			if(elements.get(i).getAttribute("data-date").equals(yesterdaysDate)){
				elements.get(i).click();
				if(elements.get(i).getAttribute("class").contains("active"))
					selectedDate = elements.get(i).getAttribute("data-date");
				Assert.assertEquals("Expcted selected as " + yesterdaysDate + ", but actually found " + selectedDate,
						yesterdaysDate, selectedDate);
				break;
			}
		}
		
/*		for(int i=0; i<elements.size(); i++)
		{
			if(elements.get(i).getAttribute("class").contains("active"))
				selectedDate = elements.get(i).getAttribute("data-date");
		}
		
		Assert.assertEquals("Expcted selected as " + yesterdaysDate + ", but actually found " + selectedDate,
				yesterdaysDate, selectedDate);	*/	
	}
	
	@Then("^user should view the \"([^\"]*)\" related to the previous \"([^\"]*)\"$")
	public void user_should_view_the_content_related_to_the_previous_date(String type, String name)
	{
		String [] scheduleTimeGroup={"morning","afternoon","evening","night"};
		if(isPresent(type, name)){
			List<WebElement> elements = new ArrayList<>();
			elements = findElements(type, name);
			for(int i=0; i<elements.size(); i++)
			{
				elements.get(i).getAttribute("id").equals(scheduleTimeGroup[i]);
				Assert.assertEquals("The content related to the date is " + elements.get(i).getAttribute("id") + ", but actually found " + scheduleTimeGroup[i],
						elements.get(i).getAttribute("id"), scheduleTimeGroup[i]);
			}
		}
	}
	@Then("^user should view the \"([^\"]*)\" related to today's \"([^\"]*)\"$")
	public void user_should_view_the_content_related_to_todays_date(String type, String name)
	{
		user_should_view_the_content_related_to_the_previous_date(type,name);
	}
	@Then("^user should view the \"([^\"]*)\" related to tomorrow's \"([^\"]*)\"$")
	public void user_should_view_the_content_related_to_tomorrows_date(String type, String name)
	{
		user_should_view_the_content_related_to_the_previous_date(type,name);
	}
@When("^user clicks on todays date from the \"([^\"]*)\" \"([^\"]*)\"$")
public void user_clicks_on_todays_date_from_the_calendar(String type, String name)
{
	refresh();
	log.info("Clicking on the Todays date " + todaysDate);
	List<WebElement> elements = new ArrayList<>();
	elements = findElements(type, name);	
	String selectedDate = null;
	for(int i=0; i<elements.size(); i++)
	{
		log.info("Trying to clicking on the Element " + elements.get(i).getAttribute("data-date"));
		if(elements.get(i).getAttribute("class").contains("active"))
			selectedDate = elements.get(i).getAttribute("data-date");
	}
	Assert.assertEquals("Expcted selected as " + todaysDate + ", but actually found " + selectedDate,
			todaysDate, selectedDate);	
}
@When("^user clicks on tomorrows date from the \"([^\"]*)\" \"([^\"]*)\"$")
public void user_clicks_on_tomorrows_date_from_the_calendar(String type, String name)
{	
	log.info("Clicking on the Tomorrows date " + tomorrowsDate);
	List<WebElement> elements = new ArrayList<>();
	elements = findElements(type, name);
	
	String selectedDate = null;
	for(int i=0; i<elements.size(); i++)
	{
		log.info("Trying to clicking on the Element " + elements.get(i).getAttribute("data-date"));
		if(elements.get(i).getAttribute("data-date").equals(tomorrowsDate)){
			elements.get(i).click();
			if(elements.get(i).getAttribute("class").contains("active"))
				selectedDate = elements.get(i).getAttribute("data-date");
			Assert.assertEquals("Expcted selected as " + tomorrowsDate + ", but actually found " + selectedDate,
					tomorrowsDate, selectedDate);
			break;
		}
	}	
/*	for(int i=0; i<elements.size(); i++)
	{
		if(elements.get(i).getAttribute("class").contains("active"))
			selectedDate = elements.get(i).getAttribute("data-date");
	}*/
}
}