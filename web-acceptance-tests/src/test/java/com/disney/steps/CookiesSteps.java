package com.disney.steps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.Normalizer;

import org.json.JSONObject;
import org.junit.Assert;

import com.disney.common.SmartNav;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;

public class CookiesSteps  extends SmartNav{

	@Given("^a regional cookie warning overlay appears at the bottom of the page$")
	public void a_regional_cookie_warning_overlay_appears_at_the_bottom_of_the_page()
	{
		Assert.assertTrue(isPresent("warning", "cookie"));		
	}
	
	@And("^cookie appears in the same language as the TLD$")
	public void cookie_appears_in_the_same_language_as_the_TLD() throws IOException
	{
		String sourceLanguage = getAttributeValue("html", "tld", "lang").substring(0, 2);
		String cookieMsg = URLEncoder.encode(URLEncoder.encode(getText("warning", "message")),"UTF-8");
		URL url = new URL("http://ws.detectlanguage.com/0.2/detect?q=" + cookieMsg + "&key=demo");
	    InputStream is = url.openConnection().getInputStream();
	    BufferedReader reader = new BufferedReader( new InputStreamReader(is, Charset.forName("UTF-8")));
	    String jsonText = readAll(reader);
	    JSONObject json = new JSONObject(jsonText);
	    JSONObject data = json.getJSONObject("data");
	    JSONObject detections = data.getJSONArray("detections").getJSONObject(0);
	    String responseLanguage = detections.get("language").toString();
	    Assert.assertTrue("Expected the TLD language as " + sourceLanguage + ", but actually found " +responseLanguage, 
	    		responseLanguage.equals(sourceLanguage));   
	}
	
	@And("^cookie contains a link to the Disney \"([^\"]*)\" Policy page in that language$")
	public void cookie_contains_a_link_to_the_Disney_Privacy_Policy_page_in_that_language(String policyName)
	{	
		Assert.assertTrue("Expected the policy page as " + policyName + " but, actually found " + getAttributeValue("warning", "cookielinktext", "href"), 
				getAttributeValue("warning", "cookielinktext", "href").contains(policyName));
	}
	
	@And("^cookie contains a clickable Accept button$")
	public void cookie_contains_a_clickable_Accept_button()
	{
		clickElement("warning", "cookielinktext");
	}
	
	@And("^user clicks Accept button$")
	public void user_clicked_the_Accept_button()
	{
		if(isPresent("warning", "cookie"))
		{
			if(!getAttributeValue("warning", "cookie", "style").equals("display: none;"))
				clickButton("cookieok");
		}
	}
	
	@Then("^a regional cookie warning overlay does not appear at the bottom of the page$")
	public void a_regional_cookie_warning_overlay_does_not_appear_at_the_bottom_of_the_page()
	{
		Assert.assertTrue("Validating the warning cookie is not present", getAttributeValue("warning", "cookie", "style").equals("display: none;"));
	}
	
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1)
	      sb.append((char) cp);
	    return sb.toString();
	}	
}
