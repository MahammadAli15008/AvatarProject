package com.disney.steps;

import static com.disney.steps.RunnerTest.environment;
import static io.restassured.RestAssured.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.Assert;
import org.xml.sax.SAXException;
import com.disney.utilities.managers.ExternalPropertiesManager;
import com.disney.common.SmartNav;
import io.restassured.response.Response;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class DisneyAPIUrlsSteps extends SmartNav {

	StringBuilder unSuccessfulURLs = new StringBuilder();
	StringBuilder successfulURLs = new StringBuilder();
	int successfulUrlsCount = 0;
	int unSuccessfulUrlsCount = 0;
	Response res;

	public ExternalPropertiesManager provideExternalProperties() {
		return new ExternalPropertiesManager();
	}

	@When("^user sends a GET request for the list of FE Urls$")
	public void user_sends_a_GET_request_for_the_list_of_FE_Urls()
			throws ParserConfigurationException, SAXException, IOException {
		environment = provideExternalProperties().getExternalEnvironmentProperty().toLowerCase();
		File currDir = new File(".");
		String dirPath = currDir.getAbsolutePath().replace(".", "");
		dirPath = dirPath + "src\\test\\resources\\com\\disney\\properties\\API_urls_" + environment + ".txt";
		BufferedReader br = null;
		FileReader fr = null;
		fr = new FileReader(dirPath.replace("\\", File.separator));
		br = new BufferedReader(fr);
		String url;
		while ((url = br.readLine()) != null) {
			if (!url.equals("")) {
				if (url.contains("YYYYMMDD")) {
					String d = String.valueOf(LocalDate.now()).replace("-", "");
					url = url.replace("YYYYMMDD", d);
					res = given().when().header("Pragma", "no-cache").get(url);
				} else
					res = given().when().header("Pragma", "no-cache").get(url);
				if (res.getStatusCode() != 200) {
					unSuccessfulURLs.append(url + " = " + res.getStatusCode() + "\n");
					unSuccessfulUrlsCount++;
				} else {
					successfulURLs.append(url + " = " + res.getStatusCode() + "\n");
					successfulUrlsCount++;
				}
			}
		}

		log.info("Total no of urls executed: " + (successfulUrlsCount + unSuccessfulUrlsCount));
		log.info("No of Urls with successful status code: " + successfulUrlsCount);
		log.info("successful FE Url's " + successfulURLs);
		br.close();
	}

	@And("^list of FE Urls status should be 200$")
	public void list_of_FE_urls_status_code() {
		Assert.assertTrue("Expected the list of FE Urls status code should be 200, but actually found "
				+ unSuccessfulUrlsCount + " unsuccessful Url's \n" + unSuccessfulURLs, unSuccessfulUrlsCount == 0);
	}

	@When("^user sends a GET request for the FE \"([^\"]*)\"$")
	public void user_sends_a_GET_request_for_the_FE(String url)
			throws ParserConfigurationException, SAXException, IOException {

		if (!url.equals("")) {
			if (url.contains("YYYYMMDD")) {
				String d = String.valueOf(LocalDate.now()).replace("-", "");
				url = url.replace("YYYYMMDD", d);
				res = given().when().header("Pragma", "no-cache").get(url);
			} else
				res = given().when().header("Pragma", "no-cache").get(url);
			log.info("Request submitted!!");
			if (res.getStatusCode() != 200) {
				unSuccessfulURLs.append(url + " = " + res.getStatusCode() + "\n");
				log.info("unsuccessful FE Url's " + unSuccessfulURLs);
			} else {
				successfulURLs.append(url + " = " + res.getStatusCode() + "\n");
				log.info("successful FE Url's " + successfulURLs);
			}
		}
	}

	@And("^the FE Url status should be (\\d+)$")
	public void the_FE_Url_status_should_be(int status) {
		Assert.assertTrue("Expected the FE Url status code to be 200, but actually found " + unSuccessfulURLs,
				unSuccessfulUrlsCount == 0);
	}
}
