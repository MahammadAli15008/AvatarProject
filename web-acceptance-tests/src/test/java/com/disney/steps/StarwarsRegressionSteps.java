package com.disney.steps;

import org.junit.Assert;
import com.disney.common.SmartNav;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import static com.disney.steps.RunnerTest.tree;
import java.util.List;

@SuppressWarnings("unused")
public class StarwarsRegressionSteps extends SmartNav {
	public static String href;
	public static int link = 0;

	@Then("^\"([^\"]*)\" is displayed$")
	public void isDisplayed(String element) {
		waitFor(2000);// wait for page redirect
		if (element.contains("error")) {
			boolean isErrorPage = isPresent("page", element);
			if (isErrorPage)
				log.info("Error page conatian the text: " + getText("text", "errorpage"));
			Assert.assertTrue("The " + element + " is not present", isErrorPage);
		} else {
			if (element.contains("Games"))
				try {
					tree.focusElement("input", "gamesdetails");
					clickElement("input", "gamesdetails");
				} catch (WebDriverException e) {
					if (tree.getCurrentUrl().contains("games"))
						log.info("Games details are available..");
				}
			Assert.assertTrue("The " + element + " is not present", isPresent("input", element));
			log.info(element + " is being displayed successfully.");
		}
	}

	@When("^user clicks on \"([^\"]*)\"$")
	public void userClicksOn(String element) {
		if (element.contains("submenu")) {
			String menuText = tree.getAttribute("page", "redirect", "class");
			String elementTrim = sanitize(element).replace("submenu", "");
			while (!menuText.contains(elementTrim)) {
				clickElement("input", element);
				menuText = tree.getAttribute("page", "redirect", "class");
				waitFor(2000);// wait for redirect to the page
			}
		} else {
			tree.focusElement("input", sanitize(element));
			try {
				tree.click("input", sanitize(element), 2000);
			} catch (WebDriverException e) {
				clickElementByScroll("input", element);
			}
		}
		log.info(element + " clicked successfully.");
		if (tree.isPresent("cookie", "close", 2000))
			clickElement("cookie", "close");
	}

	@When("^user clicks on the \"([^\"]*)\" refine search$")
	public void userClicksOnTheRefineSearch(String element) {
		if (tree.isPresent("cookie", "close", 2000))
			clickElement("cookie", "close");
		for (int i = 0; i < findElements("search", "refinetype").size(); i++) {
		log.info("Search text content count = " + findElements("search", "refinetype").size());
			String contentText = findElements("search", "refinetype").get(i).getAttribute("class");
			if (contentText.contains(sanitize(element))) {
				log.info("Search term contains" + "'" + element + "'" + "the results");
				String typeText = findElements("search", "refinetype").get(i).getText();
				String trimText[] = typeText.replace("[", "").replace("]", "").split("(?<=\\D)(?=\\d)");
				log.info("'" + element + "'" + " content contains the " + trimText[1] + " search results");
				link = Integer.parseInt(trimText[1]);
				findElements("search", "refinetype").get(i).click();
				boolean showMore = isPresentWithWait("search", "showmore", 1000);
				while (showMore) {
					tree.focusElement("search", "showmore");
					clickElement("search", "showmore");
					log.info("showmore button  clicked");
					waitFor(3000);// wait for scroll down the page
					showMore = isPresentWithWait("search", "showmore", 2000);
				}
			}
		}
	}

	@Then("^search results only displays \"([^\"]*)\" content results$")
	public void searchResultsOnlyDisplaysContentResults(String element) {
		int actualCount = findElements("search", "resultstitle").size();
		Assert.assertTrue("'" + element + "'" + " content results are not showing properly", (actualCount == link));
		log.info("'" + element + "'" + " Content displayed results are : " + actualCount);
	}

	@And("^user enters the text \"([^\"]*)\" in the \"([^\"]*)\"$")
	public void userEntersTheText(String input, String element) {
		inputText(element, input);
		waitFor(1000);// wait for dropdown list
		log.info("'" + input + "'" + " text is ready to be search");
	}

	@And("^user enters the text \"([^\"]*)\" in the \"([^\"]*)\" and press Enter$")
	public void userEntersTheTextAndPressEnter(String input, String element) {
		inputTextWithEnter(element, input);
		waitFor(1000);// wait for page redirect
		log.info("'" + input + "'" + " text is ready to be search");
	}

	@Then("^search results page contains the search text \"([^\"]*)\"$")
	public void searchResultsPageContainsTheSearchText(String text) {
		if (getCurrentURL().contains(text)) {
			int textCount = 0;
			log.info("user redirected to the " + getCurrentURL() + " page");
			for (int i = 0; i < findElements("search", "resultstitle").size(); i++) {
				String resultsText = findElements("search", "resultstitle").get(i).getText();
				if (resultsText.toLowerCase().contains(text))
					textCount++;
				waitFor(150);// wait for to get the text
			}
			Assert.assertTrue("Search results page not contain the " + "'" + text + "'" + " search term",
					findElements("search", "resultstitle").size() == textCount);
			log.info("All search results contain the search term " + "'" + text + "'");
		}
	}

	@And("^\"([^\"]*)\" dropdown displays and contains the \"([^\"]*)\" results$")
	public void autoCompleteDropdownDisplaysAndContainsTheResults(String element, String results) {
		waitFor(3000);// wait for page load
		boolean isDisplayed = isPresent("dropdown", element);
		String resultsText = "";
		if (isDisplayed) {
			for (int i = 0; i < findElements("list", sanitize(element)).size(); i++) {
				String resultsType = findElements("list", sanitize(element)).get(i).getAttribute("class");
				resultsText = findElements("list", "resultstext").get(i).getAttribute("data-linkinput");
				log.info(resultsType + " contains the " + "'" + resultsText + "'" + " as a result text ");
		}
		}				
		log.info(results + " ----contains the-- " + "'" + resultsText + "'" + " as a result text ");

		Assert.assertTrue("Results not contains the search text.", resultsText.contains(results));
		log.info("Auto complete dropdown contains all the input search results... ");
	}
	@And("^\"([^\"]*)\" are displayed$")
	public void areDisplayed(String category) {
		waitFor(2000);// wait for page redirect
		List<WebElement> elements = findElements("input", sanitize(category));
		Assert.assertTrue("No category is present.", elements.size() > 0);
		log.info("The categories are present.");
	}

	@Then("^user views archive article$")
	public void userViewsArchiveArticle() {
		String parentWindow = getWindowHandle();
		for (String childWindowHandle : getWindowHandles()) {
			// The article sometimes displayed on parent window and sometimes
			// opens up in new child window.
			// The code below handles both the ways
			if (!childWindowHandle.equals(parentWindow)) {
				switchToWindow(childWindowHandle);
				waitFor(2000);
				Assert.assertTrue("The archive article is not present.",
						findElements("newsmenu", "archivearticle").get(0).isDisplayed());
				log.info("The archive article is present.");
				closeNewtab();
				switchToWindow(parentWindow);
				break;
			} else {
				waitFor(2000);// wait for the page load
				Assert.assertTrue("The archive article is not present.",
						findElements("newsmenu", "archivearticle").get(0).isDisplayed());
				log.info("The archive article is present.");
			}
		}
	}

	@When("^user clicks on \"([^\"]*)\" from \"([^\"]*)\" list$")
	public void userClicksOnFromList(String itemElement, String itemList) {
		scrollDown();
		if (tree.isPresent("cookie", "close", 2000))
			clickElement("cookie", "close");
		String footerElement = sanitize(itemElement);
		List<WebElement> elements = findElements("input", sanitize(itemList));
		for (int i = 0; i < elements.size(); i++) {
			String footerText = sanitize(elements.get(i).getText());
			if (footerText.contains(footerElement)) {
				log.info("Footer Element to be clicked: " + itemElement);
				waitFor(1000);// wait for element to be visible
				focusElement(elements.get(i));
				elements.get(i).click();
				waitFor(10000);// Wait time after clicking the footer link to move to the next
				log.info("Clicked on " + itemElement);
				break;
			}
		}
	}

	@Then("^user is navigates to \"([^\"]*)\" page$")
	public void userNavigatesToPage(String pageElement) {
		boolean isFound = false;
		waitFor(2000);// wait for page navigation
		String parentWindow = getWindowHandle();
		for (String childWindowHandle : getWindowHandles()) {
			if (!childWindowHandle.equals(parentWindow)) {
				switchToWindow(childWindowHandle);
				log.info("Navigated to " + pageElement + " page successfully.");
				isFound = isPresent("element", pageElement);
				if (isFound)
					Assert.assertTrue("The element of " + pageElement + " is not present.",
							isPresent("element", pageElement));
				else {
					log.info("Looking for the alternative element....");
					tree.executeScript("return typeof(arguments[0]) != 'undefined' && arguments[0] != null;",
							pageElement);
				}
				log.info("The element of " + pageElement + " is present.");
				closeNewtab();
				switchToWindow(parentWindow);
				break;
			}
		}
	}

	@Then("^user view multiple \"([^\"]*)\" channels$")
	public void userViewMultipleChannels(String channel) {
		List<WebElement> channels = findElements("input", sanitize(channel));
		log.info("No Of channels ------ " + channels.size());
		for (int i = 0; i < channels.size(); i++)
			log.info("Channel" + " " + (i + 1) + ": " + channels.get(i).getText());
		Assert.assertTrue("No channels are present.", !channels.isEmpty());
	}

	@And("user clicks on \"([^\"]*)\" to play the video$")
	public void userClicksOnPlayIconToPlayVideo(String icon) {
		waitFor(1000);// wait for the video play icon
		boolean isPlay = clickPlay();
		if (isPlay)
			log.info("Video is playing");
		else
			log.info("Video is not playing");
		Assert.assertTrue("Video is unable to play", isPlay);
	}

	@And("user select the \"([^\"]*)\" from the playlist$")
	public void userSelectTheVideoFromThePlaylist(String video) {
		href = getAttributeValue("text", "watchvideo", "href");
		clickElement("list", video);
		log.info("Selected Video is clicked");
	}

	@Then("user navigates to the \"([^\"]*)\" page of the video$")
	public void userNavigatesToThePage(String watchvideo) {
		waitFor(1000);// wait for the page redirect
		getCurrentURL();
		Assert.assertTrue("Unable to navigate to the watch video page ", getCurrentURL().contains(href));
	}

	@And("^user clicks on the \"([^\"]*)\"$")
	public void userClicksOnTheArticle(String articleType) {

		if (sanitize(articleType).contains("series")) {
			waitFor(10000);
			log.info("articleType contains Series");
			List<WebElement> totalSeriesArticleLinks = findElements("input", sanitize("seriesarticlelinks"));
			waitFor(3000);
			log.info("No of links = " + totalSeriesArticleLinks.size());
			for (int i = 0; i < totalSeriesArticleLinks.size(); i++) {
				href = totalSeriesArticleLinks.get(i).getAttribute("href");
				log.info("link " + totalSeriesArticleLinks.get(i).getAttribute("href"));
				focusElement(totalSeriesArticleLinks.get(i));
				clickElement(totalSeriesArticleLinks.get(i));
				waitFor(5000);// wait for the article page to load
				log.info("Clicked on " + articleType + " successfully.");
				break;
			}
		} else {
			List<WebElement> articles = findElements("input", sanitize("article"));
			List<WebElement> articlelinks = findElements("input", sanitize("articlelink"));
			link: for (int i = 0; i < articles.size(); i++) {
				String trim = sanitize(articles.get(i).getText());
				if (sanitize(articleType).contains("community")) {
					href = articlelinks.get(i).getAttribute("data-slug");
				} else
					href = articlelinks.get(i).getAttribute("href");
				if (sanitize(articleType).contains(trim) && !sanitize(trim).isEmpty()) {
					focusElement(articlelinks.get(i));
					clickElement(articlelinks.get(i));
					waitFor(2000);// wait for the article page to load
					if (isPresent("title", articleType))
						log.info("Clicked on " + articleType + " successfully.");
					break link;
				}
			}
		}
	}

	@And("^\"([^\"]*)\" page should get displayed$")
	public void newsArticlePageShouldGetDisplayed(String articleType) {
		if (articleType.contains("community")) {
			String secondWindow = getWindowHandles().get(0);
			switchToWindow(secondWindow);
			log.info("User redirected to the community site: " + getCurrentURL());
			Assert.assertTrue("Unable to navigate to the " + articleType + " page ",
					sanitize(getCurrentURL()).contains(sanitize(href).substring(0, 3)));
		} else {
			getCurrentURL();
			Assert.assertTrue("Unable to navigate to the " + articleType + " page ", getCurrentURL().contains(href));
		}
	}

	@When("user navigated to \"([^\"]*)\" page$")
	public void userNavigatedToThePage(String endpoint) {
		waitFor(1000);// wait for the page redirect
		getCurrentURL();
		tree.navigateToUrl(getCurrentURL() + endpoint);
		log.info("Url of the current page: " + getCurrentURL());
	}

	@And("user clicks on any of the \"([^\"]*)\" content results$")
	public void userClicksOnAnyOfTheContentResults(String content) {
		for (int i = 0; i < findElements("list", "autocomplete").size(); i++) {
			String resultsType = findElements("list", "autocomplete").get(i).getAttribute("class");
			if (resultsType.contains(sanitize(content))) {
				href = findElements("list", "resultstext").get(i).getAttribute("href");
				log.info("Selected result link:" + href);
				findElements("list", "autocomplete").get(i).click();
				log.info("Clicked on the " + content + " search result content");
				break;
			}
		}
	}

	@Then("user navigate to the selected \"([^\"]*)\" result page$")
	public void userNavigatedToTheSelectedPage(String content) {
		waitFor(2000);// wait for the page redirect
		getCurrentURL();
		log.info("Url of the current page: " + getCurrentURL());
		Assert.assertTrue("Unable to navigate to the " + content + " page ", getCurrentURL().contains(href));
	}

	@When("the user enters the starwars \"([^\"]*)\" URL$")
	public void theUserEntersTheStarwarsURL(String endpoint) {
		waitFor(1000);// wait for the page redirect
		log.info("Current Url: " + endpoint);
		tree.navigateToUrl(endpoint);
	}

	@Then("user redirect to the \"([^\"]*)\" page$")
	public void userRedirectToThePage(String endpoint) {
		waitFor(3000);// wait for the page redirect
		log.info("Url of the current page after navigation: " + getCurrentURL());
		Assert.assertTrue("User not redirected to the Starwars US page",
				tree.getDriver().getTitle().contains("StarWars.com"));
	}
}