package com.disney.steps;

import com.disney.common.SmartNav;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class GlobalChromeSteps extends SmartNav{

		
	@And("^the user clicks on the Disney \"([^\"]*)\" button$")
    public void theUserClicksOnTheButton(String dropDownButton)
    {		
		/*if(isPresent("warning", "cookie"))
		{
			if(!getAttributeValue("warning", "cookie", "style").equals("display: none;"))
			{
				clickButton("cookieok");
			}
		}*/
        clickButton(dropDownButton);        
    }
	
	@Then("^the user is able to click on the Disney \"([^\"]*)\" button to minimize the Chrome$")
	public void the_user_is_able_to_click_on_the_Disney_up_arrow_to_minimize_the_Chrome(String upArrowButton) {

		/*if(isPresent("warning", "cookie"))
		{
			if(!getAttributeValue("warning", "cookie", "style").equals("display: none;"))
				clickButton("cookieok");
		}*/
		clickButton(upArrowButton);		
	}
	
	@Then("^the \"([^\"]*)\" \"([^\"]*)\" expands open$")
	public void the_menu_expands_open(String name, String type) {
		
		Assert.assertTrue("Expected the element to expand the chrome menu, but actually not found", isPresent(type,name));
	}
	
	@Then("^user clicks on the \"([^\"]*)\" \"([^\"]*)\" and will be navigated to the related page$")
	public void user_clicks_on_the (String name, String type) {
		
		List<WebElement> elements = new ArrayList<>();
		elements = findElements(type, name);
		
		System.out.println(getAttributeValue(type, name, "href"));
		String menuItems = elements.get(0).getText();
		String [] menuItemsList = menuItems.split("\n");

		for(int i=0; i<menuItemsList.length; i++)
		{			
			log.info("Clicking on the link " + menuItemsList[i]);
			waitFor(1000);
			findElement(By.linkText(menuItemsList[i]));
			navigateBack();
			waitFor(1000);
			clickButton("chrome dropdown"); 
		}
		navigateBack();
	}
	
	@Then("^the user will be navigated to \"([^\"]*)\" page with related content$")
	public void navigate_page(String pageName) {		
		Assert.assertTrue("Validating the url containg the name " + pageName + " but, Actually found " + getCurrentURL(), 
				getCurrentURL().contains(pageName));
	}
}
