package com.disney.steps;

import static com.disney.steps.RunnerTest.environment;
import static io.restassured.RestAssured.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.Assert;
import org.xml.sax.SAXException;
import com.disney.utilities.managers.ExternalPropertiesManager;
import com.disney.common.SmartNav;
import io.restassured.response.Response;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class ShopDisneyApiUrlsSteps extends SmartNav {

	Response response;
	StringBuilder unSuccessfulURLs = new StringBuilder();
	StringBuilder successfulURLs = new StringBuilder();
	HashMap<String, Integer> unSuccessfulResponseURLs = new HashMap<String, Integer>();
	HashMap<String, Integer> successfulResponseURLs = new HashMap<String, Integer>();

	public ExternalPropertiesManager provideExternalProperties() {
		return new ExternalPropertiesManager();
	}

	@When("^user sends a GET request for the list of Shop Disney Urls$")
	public void user_sends_a_GET_request_for_the_list_of_shop_disney_urls()
			throws ParserConfigurationException, SAXException, IOException {
		environment = provideExternalProperties().getExternalEnvironmentProperty().toLowerCase();

		File currDir = new File(".");
		String dirPath = currDir.getAbsolutePath().replace(".", "");

		dirPath = dirPath + "src\\test\\resources\\com\\disney\\properties\\ShopDisney_API_Urls_" + environment
				+ ".txt";

		BufferedReader br = null;
		FileReader fr = null;
		fr = new FileReader(dirPath.replace("\\", File.separator));
		br = new BufferedReader(fr);

		String url;
		while ((url = br.readLine()) != null) {
			if (!url.equals("")) {

				response = given().when().get(url);
				if (response.getStatusCode() != 200)
					unSuccessfulURLs.append(url + " = " + response.getStatusCode() + "\n");
				else
					successfulURLs.append(url + " = " + response.getStatusCode() + "\n");
			}
		}
	}

	@And("^list of Shop Disney Urls status should be 200$")
	public void list_of_shop_disney_urls_status_code() {
		String unSuccessfulURLsSize[] = unSuccessfulURLs.toString().split("https:");
		String successfulURLsSize[] = successfulURLs.toString().split("https:");
		log.info("Successful Shop Disney Url's " + successfulURLs);
		log.info("unSuccessful Shop Disney Url's:" + unSuccessfulURLs);
		log.info("Total no of urls executed: " + ((successfulURLsSize.length - 1) + (unSuccessfulURLsSize.length - 1)));
		log.info("No of Urls with successful status code: " + (successfulURLsSize.length - 1));
		log.info("No of Urls failed: " + (unSuccessfulURLsSize.length - 1));
		Assert.assertTrue(
				"Expected the list of Shop Disney Urls status code should be 200, but actually found "
						+ (unSuccessfulURLsSize.length - 1) + " unSuccessfulURLs" + "\n" + unSuccessfulURLs,
				unSuccessfulURLs.length() == 0);
	}
}
