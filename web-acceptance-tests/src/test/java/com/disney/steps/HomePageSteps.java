package com.disney.steps;

import static com.disney.steps.RunnerTest.tree;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;
import com.disney.common.SmartNav;
import com.disney.config.WebConfig;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class HomePageSteps extends SmartNav {

	private String collectionURL = null;
	private WebConfig config = new WebConfig();

	@Given("^the user is on the Search \"([^\"]*)\" page$")
	public void theUserIsOnTheSearchPage(String collection) {

		try {
			collectionURL = config.provideWorkingEnvironment(collection);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		tree.setBaseUrl(collectionURL);
		log.info("Navigating to the page for the collection type " + collection + " with the URL " + collectionURL);
		navigateTo(collectionURL);
		setPage("home");
		myIdLogin();

		Assert.assertEquals("Login unsuccessful, Welcome user is not displayed.",
				"Welcome : " + System.getProperty("username").toUpperCase(), getText("element", "welcomeuser"));
	}

	@When("^user searches for \"([^\"]*)\" collection dropdown$")
	public void userSearchesForCollection(String collection) {

		clickElement("button", "collectiondd");
		waitFor(200);
		clickElement("click", "collectionsearch");
		waitFor(200);
		inputText("collectionsearch", collection);
		waitFor(400);
	}

	@And("^user clicks on the search results$")
	public void clicksOnSearchResults() {

		clickElement("click", "disneyus");
		waitFor(500);
	}

	@Then("^user should be redirected to admin page for \"([^\"]*)\" collection$")
	public void userShouldBeOnAdminPageForSelectedCollection(String collection) {

		Assert.assertTrue(
				"Expecting user is on " + collection + " admin page , actually found "
						+ getText("header", "disneyusTitle"),
				collection.equalsIgnoreCase(getText("header", "disneyusTitle")));

		log.info("User is successfully redirected to the " + collection + " Collection page");
	}

	@Then("^the auto complete results should contain \"([^\"]*)\"$")
	public void autoCompleteResults(String searchKey) {

		List<WebElement> list = findElements("element", "autosearchresults");
		log.info("Validating the auto complete search results!");
		for (WebElement e : list) {
			Assert.assertTrue(
					"Expecting auto complete results contains " + searchKey + " , actually found " + e.getText(),
					searchKey.equalsIgnoreCase(e.getText()));
		}
		log.info("Successfully validated the autosearch results for " + searchKey);
	}
}