package com.disney.steps;

import org.junit.Assert;
import com.disney.common.SmartNav;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import static com.disney.steps.RunnerTest.tree;
import java.util.List;

@SuppressWarnings("unused")
public class DTCIMediaRegressionSteps extends SmartNav {
	public static String href;
	public String elmType;
	public int size = 0;

	@And("^user mouse over on \"([^\"]*)\" \"([^\"]*)\"$")
	public void userMouseOverOnTheIcon(String element, String type) {
		mouseOverElement(type, element);
		waitFor(1000);// wait for page redirect
		log.info("Mouse over on the " + element + " " + type);
	}

	@And("^user move to the \"([^\"]*)\" \"([^\"]*)\"$")
	public void userMoveToTheFooterlist(String element, String type) {
		List<WebElement> footerList = tree.findElements(sanitize(type), sanitize(element));
		size = footerList.size();
		waitFor(1000);// wait for page redirect
		log.info("Footer List elements are: " + size);
	}

	@Then("^user should redirects to all \"([^\"]*)\" \"([^\"]*)\" options$")
	public void userShouldRedirectToAllDropdownOptions(String element, String type) {
		List<WebElement> optionsList;
		if (element.contains("Footer"))
			optionsList = tree.findElements("list", "dtcifooter");
		else
			optionsList = tree.findElements("list", "dtcidropdown");
		for (int i = 0; i < optionsList.size(); i++) {
			href = optionsList.get(i).getAttribute("href");
			String optionText = optionsList.get(i).getAttribute("innerText");
			optionsList.get(i).click();
			waitFor(2000);// wait for page redirect
			String parentWindow = getWindowHandle();
			String parentWindowUrl = getCurrentURL();
			String url = "";
			for (String childWindowHandle : getWindowHandles()) {
				if (!childWindowHandle.equals(parentWindow)) {
					switchToWindow(childWindowHandle);
					url = getCurrentURL();
					waitFor(2000);// wait for page redirect
					if (url.contains(href))
						log.info("User redirected to the " + element + " '" + optionText + "' : " + href + " page");
					tree.closeActiveTab();
					switchToWindow(parentWindow);
				} else
					url = getCurrentURL();
				if (url.contains(href))
					log.info("User redirected to the " + element + " '" + optionText + "' : " + href + " page");
			}
			if (element.contains("Footer"))
				optionsList = tree.findElements("list", "dtcifooter");
			else {
				tree.navigateToUrl(tree.getBaseUrl());
				waitFor(1000);// wait for page redirect
				mouseOverElement("tab", element);
				waitFor(1000);// Wait for dropdown
				optionsList = tree.findElements("list", "dtcidropdown");
				log.info("Mouse over on the " + element + " tab");
			}
		}
	}

	@When("^user is clicks on \"([^\"]*)\" \"([^\"]*)\"$")
	public void userClickedOnTheIcon(String element, String type) {
		tree.focusElement(sanitize(type), sanitize(element));
		href = tree.getAttribute(type, sanitize(element), "href");
		clickElement(type, element);
		elmType = type;
		waitFor(1000);// wait for page redirect
		log.info("clicked on the " + element + " " + type);
	}

	@When("^user clicks on \"([^\"]*)\" icon$")
	public void userClickedOnTheSearchIcon(String element) {
		if (isPresent("icon", element))
			;
		clickElement("icon", element);
		log.info("clicked on the " + element + " " + "icon");
	}

	@And("^user enters the search text \"([^\"]*)\" and clicks on \"([^\"]*)\" button$")
	public void userEntersTheSearchInput(String text, String element) {
		waitFor(1000);
		tree.getElement("input", "dtcisearch").sendKeys(text);
		if (element.contains("ENTER")) {
			tree.getElement("input", "dtcisearch").sendKeys(Keys.ENTER);
		} else {
			clickElement("button", "submit");
		}
		tree.getElement("input", "dtcisearch").sendKeys(text);
		clickElement("button", "submit");
		log.info("'" + text + "' is entered as Search Text ");
	}

	@Then("^user should redirects to \"([^\"]*)\" page$")
	public void userRedirectsToTheHomePage(String element) {
		String parentWindow = getWindowHandle();
		String parentWindowUrl = getCurrentURL();
		String childWindowUrl = "";
		for (String childWindowHandle : getWindowHandles()) {
			if (!childWindowHandle.equals(parentWindow)) {
				switchToWindow(childWindowHandle);
				childWindowUrl = getCurrentURL();
			} else
				childWindowUrl = getCurrentURL();
		}
		log.info("user redirects to the url: " + getCurrentURL());
		String url = getCurrentURL();
		if (url.contains(href))
			Assert.assertTrue("user is not redirected to a valid URL", isPresent(elmType, sanitize(element)));
	}	
	
	@Then("^user should redirects to the DTCI \"([^\"]*)\" page$")
	public void user_should_redirects_to_page_new(String element) throws Throwable {
		log.info("user redirects to the url: " + getCurrentURL());
		String url = getCurrentURL();
		Assert.assertTrue("user is not redirected to a valid URL", isPresent(elmType, sanitize(element)));
	}

	@And("^the user navigates to the \"([^\"]*)\" url$")
	public void theUserNavigatesToTheUrl(String url) {
		waitFor(2000);// wait for page load
		tree.navigateToUrl(url);
		log.info("User navigates to the url: " + url);
	}

	@Then("^user should redirects to search \"([^\"]*)\" page$")
	public void userRedirectsToTheSearchPage(String element) {
		String currentUrl = getCurrentURL();
		log.info("user redirects to the url: " + currentUrl);
		if (currentUrl.contains("search?q"))
			Assert.assertTrue("user is not redirected to a valid URL", isPresent("link", sanitize(element)));
	}

	@And("^user clicks on \"([^\"]*)\" (\\d+) \"([^\"]*)\"$")
	public void userClicksOnTheSearchLink(String element, int index, String type) {
		List<WebElement> searchList = tree.findElements(sanitize(type), sanitize(element));
		href = searchList.get(index - 1).getAttribute("href");
		log.info("No Of links: " + searchList.size());
		searchList.get(index - 1).click();
		waitFor(5000);// wait for page redirect
		log.info("Clicked on " + index + " Search Link");
	}

	@Then("^user should redirects to (\\d+) link \"([^\"]*)\" page$")
	public void userRedirectsToTheSearchResultsPage(int index, String element) {
		String currentUrl = getCurrentURL();
		log.info("user redirects to the url: " + currentUrl);
		Assert.assertTrue("user is not redirected to " + index + " URL", currentUrl.contains(href));
	}

	@And("^user should clicks on \"([^\"]*)\" button$")
	public void userClickedOnTheShowMoreButton(String element) {
		boolean isDisplayed = false;
		boolean isClickable = false;
		tree.scrollDown();
		if (element.contains("Show")) {
			isDisplayed = tree.isAttributeValueContained("button", "showmore", "class", "show_more blue button large ada-el-focus");
			while (isDisplayed) {
				clickElement("button", element);
				waitFor(2000);// wait for show more button
				isDisplayed = tree.isAttributeValueContained("button", "showmore", "class", "show_more blue button large ada-el-focus", 5000);
				isClickable = true;
				break;
			}
			Assert.assertTrue("User is unable to click on the " + element + " button", isClickable);
			log.info("clicked on the " + element + " " + "button");
		}
	}

	@Then("^the images should have alt or aria-label tag$")
	public void allTheImagesShouldHaveAltTag() {
		List<WebElement> image = tree.findElements("tag", "image");
		int counterAriaLabel = 0;
		log.info("total images: " + image.size());
		for (WebElement el : image) {
			// Filter out the ctologger images
			tree.waitFor(100);//wait for sync
			// If alt is empty or null, add one to counter
			if (el.getAttribute("alt") == null || el.getAttribute("alt").equals("")) {
				if (el.getAttribute("aria-label") == null || el.getAttribute("aria-label").equals("")) {
					log.info("The following image does not have an aria-label and alt tag: " + el.getAttribute("src"));
					counterAriaLabel++;
				}
			}
		}
		Assert.assertTrue("All the images are having either alt or aria-label attribute.", (counterAriaLabel <= 2));
	}
	
	@When("^the user navigates to the \"([^\"]*)\"$")
	public void the_user_navigates_to_the(String url) throws Throwable {
		waitFor(2000);// wait for page load
		tree.navigateToUrl(url);
		log.info("User navigates to the url: " + url);	   
	}
}