package com.disney.steps;

import static com.disney.steps.RunnerTest.tree;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONObject;
import org.xml.sax.SAXException;

import com.disney.common.SmartNav;
import com.disney.config.WebConfig;
import com.disney.utilities.managers.TestDataManager;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class DisableSpellCheckSteps extends SmartNav {

	private String collectionURL = null;
	private WebConfig config = new WebConfig();

	@Given("^the user is on DisneyUS search url having spell check disabled$")
	public void searchUrl() {

		try {
			collectionURL = config.provideWorkingEnvironment("disablespellcheck");
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	@When("^the user performs search with incorrect keyword \"([^\"]*)\"$")
	public void performSearch(String keyword) {

		String url = collectionURL + "?q=" + keyword + TestDataManager.DISABLESPELLCHECK_ENDPOINT;
		tree.setBaseUrl(url);
		navigateTo(url);
		setPage("home");
	}

	@Then("^the response should suggest correct keyword \"([^\"]*)\"$")
	public void verifySpellCheckResponse(String suggestedKeyword) {

		log.info("Verifying the values for the keys " + TestDataManager.DISABLESPELLCHECK_KEYWORD1 + " and "
				+ TestDataManager.DISABLESPELLCHECK_KEYWORD2 + " in the response.");
		String json = getText("text", "spellcheckresponse");
		JSONObject obj = new JSONObject(json);

		Assert.assertEquals(suggestedKeyword, obj.getString(TestDataManager.DISABLESPELLCHECK_KEYWORD1));
		Assert.assertFalse((boolean) obj.get(TestDataManager.DISABLESPELLCHECK_KEYWORD2));
	}
}
