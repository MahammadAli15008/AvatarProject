package com.disney.steps;

import com.disney.common.StarwarsStepHelpers;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static com.disney.steps.RunnerTest.tree;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
public class StarwarsRegressionVideoSteps extends StarwarsStepHelpers {

	private final Logger log = LoggerFactory.getLogger(StarwarsRegressionVideoSteps.class);

	@When("^user is navigates to the \"([^\"]*)\" page$")
	public void userNavigatesToVideoPage(String endpoint) {
		if (endpoint.contains("embed")) {
			tree.navigateToUrl(getCurrentURL() + "embed" + "/5851dba7c9983ebd6b4f2a72");
			tree.click("button", "poster");
			tree.getDriver().manage().window().maximize();
		} else if (endpoint.contains("audio")) {
			tree.navigateToUrl(getCurrentURL() + "tv-shows" + "/star-wars-rebels/rebel-resolve-episode-guide");
			tree.focusElement("button", "poster");
			tree.click("button", "poster");
		} else {
			tree.navigateToUrl(getCurrentURL() + "video"
					+ "/the-star-wars-show-live-announced-and-darth-revan-in-galaxy-of-heroes");
			log.info("Navigating to the video page " + getCurrentURL());
		}
	}

	@And("^the video player is loaded$")
	public void theVideoPlayerIsLoaded() {
		log.info("-------- Waiting for the video to be loaded--------");
		isVideoPlayerLoaded();
	}

	@And("^the user clicks on the Video pause button$")
	public void theUserClicksOnTheVideoPauseButton() {
		log.info("-------- Waiting for the video to be click on pause--------");
		pause();
	}

	@Then("^the video should be paused$")
	public void theVideoShouldBePaused() {
		log.info("-------- waiting for the video to be paused--------");
		assertTrue("Video is unable to paused", isPaused());
	}

	@And("^the user clicks on the Video play button$")
	public void theUserClicksOnTheVideoPlayButton() {
		log.info("-------- Waiting for the video to be click on play button--------");
		play();
	}

	@Then("^the video should be resume to play$")
	public void theVideoShouldBeResumeToPlay() {
		log.info("-------- waiting for the video to be resume to play--------");
		assertTrue("Video is unable to resume to play", isPlaying());
	}

	@And("^the user clicks on the Video fullscreen button$")
	public void theUserClicksOnTheVideoFullscreenButton() {
		log.info("-------- Waiting for the video to be view on full screen--------");
		fullscreen();
	}

	@Then("^the video should be displayed in fullscreen$")
	public void theVideoShouldBeDisplayedInFullscreen() {
		log.info("-------- waiting for the click on Full screen--------");
		assertTrue("Video is unable to view in Fullscreen", isFullscreen());
	}

	@And("^the user clicks on the Video collapse fullscreen button$")
	public void theUserClicksOnTheVideoCollapseFullscreenButton() {
		log.info("-------- Waiting for the video to be exit from fullscreen--------");
		exitFullscreen();
	}

	@Then("^the video should be displayed in normal screen$")
	public void theVideoShouldBeDisplayedInNormalScreen() {
		log.info("-------- waiting for the click on exit fullscreen--------");
		assertTrue("Video is unable to view in Normalscreen", isNormalscreen());
	}

	@And("^the user clicks on the Video mute button$")
	public void theUserClicksOnTheVideoMuteButton() {
		log.info("-------- Waiting for the video to be mute--------");
		mute();
	}

	@Then("^the video should be mute$")
	public void theVideoShouldBeMute() {
		log.info("-------- waiting for the click on mute button--------");
		assertTrue("Video is unable to click on mute button", isMute());
	}

	@And("^the user clicks on the Video unmute button$")
	public void theUserClicksOnTheVideoUnmuteButton() {
		log.info("-------- Waiting for the video to be unmute--------");
		unmute();
	}

	@Then("^the video should be unmute$")
	public void theVideoShouldBeUnmute() {
		log.info("-------- waiting for the click on unmute button--------");
		assertTrue("Video is unable to click on unmute button", isUnmute());
	}

	@And("^the user clicks on the Video captions \"([^\"]*)\" button$")
	public void theUserClicksOnTheVideoCaptionsONButton(String captions) {
		log.info("-------- Waiting for the click on captions " + captions + " button--------");
		captionsOn(captions);
	}

	@Then("^the video should displayed with Captions$")
	public void theVideoShouldDisplayedWithCaptions() {
		log.info("-------- waiting for the video to be displayed with Captions--------");
		assertTrue("Video is unable to display the captions", isCaptionsON());
	}

	@Then("^the video should displayed without Captions$")
	public void theVideoShouldDisplayedWithOutCaptions() {
		log.info("-------- waiting for the video to be displayed without Captions--------");
		assertTrue("Video is unable to turn OFF the captions", isCaptionsOFF());
	}

	@And("^the user drags the Video player progress slider to the left$")
	public void theUserDragsTheVideoPlayerProgressSliderToTheLeft() {
		logger.info("-------- Waiting for the video to drag the scrubber--------");
		// Play the video for some time..
		waitFor(2000);
		// get initial scrub width..
		int initialScrubWidth = getScrubberWidth();
		logger.info("initialScrubWidth: " + initialScrubWidth);
		// get initial scrub time..
		String initialScrubTime = getScrubberTime();
		logger.info("initialScrubTime: " + initialScrubTime);
		// move scrubber back..
		dragScrubberLeft();
		logger.info("****Clicked on V3 Drag left Button***");
	}

	@And("^the audio player is loaded$")
	public void theAudioPlayerIsLoaded() {
		log.info("-------- Waiting for the audio to be loaded--------");
		isVideoPlayerLoaded();
	}

	@Then("^the audio should be played$")
	public void theAudioShouldBePlayed() {
		log.info("-------- waiting for the audio to be play--------");
		assertTrue("Audio is unable to play", isPlaying());
	}
}