package com.disney.steps;

import com.disney.common.SmartNav;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static com.disney.steps.RunnerTest.tree;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

public class CommunitySteps extends SmartNav {

	@Then("^user moves to the \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_moves_to(String element, String type) throws Throwable {

		waitFor(1000);
		tree.mouseOver(sanitize(type),sanitize(element));
	}
	@Then("^user can view the \"([^\"]*)\" \"([^\"]*)\" in separate window$")
	public void user_can_view_the_in_separate_window(String element, String type) throws Throwable {

		String parentWindow =tree.getDriver().getWindowHandle();
		for (String childWindowHandle : getWindowHandles()) {
			if (!childWindowHandle.equals(parentWindow)) {
				tree.getDriver().switchTo().window(childWindowHandle);}
		}
		Assert.assertTrue(element + " " + type +" is not present in current page",isPresent(sanitize(type),sanitize(element)));
	}
	@When("^user Click the play button initiates a audio play back in \"([^\"]*)\"$")
	public void user_Click_the_play_button_initiates_a_audio_play_back_in(String type) throws Throwable {
		int slidesCount = 0;
		List<WebElement> audios = tree.findElements(sanitize(type), "imagecheck");
		List<WebElement> audiosTitle = tree.findElements(sanitize(type), "titlecheck");
		List<WebElement> audiosDecscription= tree.findElements(sanitize(type), "descriptioncheck");
		List<WebElement> playbutton= tree.findElements(sanitize(type), "playbutton");
		for (int i = 0; i < audios.size(); i++) {
			waitFor(1000);//wait for element
			log.info(type + " title is: " + audiosTitle.get(i).getText());
			log.info(type + " description is: " + audiosDecscription.get(i).getText());
			elementClick(playbutton.get(i));
			waitFor(3000);//wait to listen audio
			slidesCount++;
			Assert.assertTrue("The user unable to view the " + type + " images", (slidesCount > 0));
		}
		Assert.assertTrue("All images are not displayed correctly", slidesCount==audios.size());
		log.info(type + " slide image count is: " + slidesCount);
	}
}
