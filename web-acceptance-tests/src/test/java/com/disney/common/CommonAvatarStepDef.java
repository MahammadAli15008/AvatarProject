package com.disney.common;

import static com.disney.steps.RunnerTest.tree;

import com.disney.config.WebConfig;

import cucumber.api.java.en.Given;

public class CommonAvatarStepDef extends SmartNav {
	String collectionURL=null;
	WebConfig config = new WebConfig();
	
	@Given("^user navigates to the \"([^\"]*)\" page$")
	public void userNavigatesToHomePage(String collection)
	{
		try {
			collectionURL = config.provideWorkingEnvironment(collection);
		} catch (Exception e) {
			e.printStackTrace();
		}
		tree.setBaseUrl(collectionURL);
		log.info("Navigating to the avatar home page with the URL :" + collectionURL);
		navigateTo(collectionURL);
		setPage("home");
		maximize();
		waitFor(3000);

	}

}
