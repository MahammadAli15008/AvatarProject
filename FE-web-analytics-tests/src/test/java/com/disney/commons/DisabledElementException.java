package com.disney.commons;

/**
 * Created by Trammel on 6/24/16.
 * Edited by Jared on 8/30/16.
 */
public class DisabledElementException extends RuntimeException
{
    public DisabledElementException(String s)
    {
        super(s);
    }
}
